package wang.wenlei.lifespringcloudorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LifeSpringCloudOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(LifeSpringCloudOrderApplication.class, args);
    }

}
