package wang.wenlei.lifekafkaconsumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
@SpringBootApplication
public class KafkaConsumerApplication {
	public static void main(String[] args) {SpringApplication.run(KafkaConsumerApplication.class, args);}

	@KafkaListener(topics = "my_first" ,id = "1")
	public void getMessage(String str) {
		log.info("获取到Kafka消息 ： " + str);
	}
}
