package learn.note.JSON.jacksonLearn;

import com.fasterxml.jackson.core.JsonFactory;

/**
 * @author WangWenLei
 * @DATE: 2021/4/23
 **/
public class Main {
    public static void main(String[] args) {
        // 通常从创建一个可重用的(并且是线程安全的，一旦配置)JsonFactory实例开始
        JsonFactory jsonFactory = new JsonFactory();

    }
}
