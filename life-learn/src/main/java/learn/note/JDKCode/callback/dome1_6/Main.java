package learn.note.JDKCode.callback.dome1_6;

/**
 * @Author Wang WenLei
 * @Date 2021/11/27 20:50
 * @Version 1.0
 **/
public class Main {
    public static void main(String[] args) {
        ClassA a = new ClassA();
        a.a();
    }
}
