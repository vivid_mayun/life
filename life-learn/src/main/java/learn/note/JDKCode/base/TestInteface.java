package learn.note.JDKCode.base;

/**
 * @Author Wang WenLei
 * @Date 2022/6/6 9:42
 * @Version 1.0
 **/
public interface TestInteface {
    void test();
}
