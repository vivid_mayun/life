package learn.note.JDKCode.base;

import java.util.ArrayList;
import java.util.List;

/**
 * @author WangWenLei
 * @DATE: 2022/3/1
 **/
public class BaseType {
    public static void main(String[] args) {
        byte a = 'a';
        byte a1 = 91;
        short b = 1;
        int c = 1;
        long d = 4L;
        float e = 2.0f;
        double f = 3.00d;

        char g = 'd';
        c <<= 2;

        boolean h = true;
        int [] list = new int[] {1,2,3,4,5};
        for (int i = 0; i < list.length ; i++) {

        }

        for(int i : list) {

        }

        List<String> list1 = new ArrayList<>();
        list1.forEach(item -> {

        });
    }
}
