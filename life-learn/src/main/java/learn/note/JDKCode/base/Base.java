package learn.note.JDKCode.base;

/**
 * @Author Wang WenLei
 * @Date 2022/6/6 9:42
 * @Version 1.0
 **/
public class Base implements TestInteface{

    @Override
    public void test() {

    }

    private static int f1() {
        try {
            Integer num = null;
            return 1;
        } catch (Exception e) {
            return 2;
        } finally {
            return 3;
        }
    }

    public static void main(String[] args) {
        System.out.println(f1());
    }
}
