package learn.note.JDKCode.EventListerner;

import java.util.EventObject;

/**
 * 件机制一般包括三个部分：EventObject，EventListener和Source。
 * EventObject：事件状态对象的基类，它封装了事件源对象以及和事件相关的信息。所有java的事件类都需要继承该类
 *      事件对象，自定义事件对象需要继承该类
 * EventListener：是一个标记接口，就是说该接口内是没有任何方法的。所有事件监听器都需要实现该接口。事件监听器注册在事件源
 * 上，当事件源的属性或状态改变的时候，调用相应监听器内的回调方法（自己写）。
 * @author WangWenLei
 * @DATE: 2022/1/25
 **/
public class MyEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public MyEvent(Object source) {
        super(source);
    }
}
