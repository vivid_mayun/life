package learn.note.JDKCode.EventListerner;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

/**
 * @author WangWenLei
 * @DATE: 2022/1/25
 **/
public class MySource {
    private int status;

    List<EventListener> eventListeners = new ArrayList<>();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public void addListener(EventListener listener) {
        eventListeners.add(listener);
    }

    public void notifyListeners(int oldStatus,int newStatus) {
        eventListeners.forEach(l -> {
            if (oldStatus == newStatus) {
                System.out.println("相等");
            } else {
                System.out.println("不相等");
            }
        });
    }
}
