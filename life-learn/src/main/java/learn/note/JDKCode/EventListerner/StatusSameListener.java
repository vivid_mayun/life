package learn.note.JDKCode.EventListerner;

import java.util.EventListener;

/**
 * @author WangWenLei
 * @DATE: 2022/1/25
 **/
public class StatusSameListener implements EventListener {
    public void handlerEvent(MyEvent event) {
        System.out.println(event.getSource() + "的状态没有任何变化！");
    }
}
