package learn.note.JDKCode.EventListerner;

/**
 * @author WangWenLei
 * @DATE: 2022/1/25
 **/
public class Main {
    public static void main(String[] args) {
        MySource mySource = new MySource();
        mySource.addListener(new StatusChangedListener());
        mySource.addListener(new StatusSameListener());

        int oldStatus = mySource.getStatus();
        mySource.setStatus(1);
        int newStatus = mySource.getStatus();
        mySource.notifyListeners(oldStatus,newStatus);
    }
}
