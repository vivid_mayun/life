package learn.note.JDKCode.singleton;

/**
 * @author: Wang WenLei
 * @create: 2022-07-21 14:18
 **/
public class Main {
    public static void main(String[] args) {
//        Test t1 = LazySingleton.getInitial();
//        Test t2 = LazySingleton.getInitial();
        for (int i = 0 ; i < 10 ; i++) {
            Thread t = new Thread(() -> {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                Test initial = LazySingleton.getInitial();
                System.out.println(initial);
            });
            t.start();
        }
//        System.out.println(t1 == t2);
//        System.out.println(t1.equals(t2));
//        System.out.println(t1.hashCode() == t2.hashCode());
//        System.out.println(t1.toString() + " | " + t2.toString());
    }
}
