package learn.note.JDKCode.singleton;

/**
 * @author: Wang WenLei
 * @create: 2022-07-21 14:13
 **/
public class Test {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
