package learn.note.JDKCode.singleton;

import java.io.Serializable;

/**
 * @author: Wang WenLei
 * @create: 2022-07-21 14:15
 **/
public class LazySingleton implements Serializable {
    private static final long serialVersionUID = 5665353624555779765L;
    private static volatile Test test;

    private LazySingleton () {}

    public static Test getInitial () {
        if (test == null) {
            synchronized (LazySingleton.class) {
                if (test == null) {
                    test = new Test();
                }
            }
        }
        return test;
    }


}
