package learn.note.JDKCode.singleton;

/**
 * 懒汉模式返回Test单例对象
 * @author: Wang WenLei
 * @create: 2022-07-21 14:11
 **/
public class HungrySingleton {
    private static final Test test = new Test();

    private HungrySingleton () {}

    public static Test getInitial() {
        return test;
    }
}
