package learn.note.JDKCode.singleton;

/**
 * 一种神仙写法
 * @author: Wang WenLei
 * @create: 2022-07-21 14:44
 **/
public class LazySingleton_Enum {
    private LazySingleton_Enum () {}

    private enum Singleton {
        INSTANCE;
        private final LazySingleton_Enum instance;
        Singleton() {
            instance = new LazySingleton_Enum();
        }
        private LazySingleton_Enum getInstance() {
            return instance;
        }
    }

    public static LazySingleton_Enum getInstance() {
        return Singleton.INSTANCE.getInstance();
    }

}
