package learn.note.JDKCode.JVM;

/**
 * @Author Wang WenLei
 * @Date 2022/8/25 21:58
 * @Version 1.0
 **/
public class Bar {
    int a = 1;
    final static int b = 2;
    static int c = 2;
    public int sum(int c) {
        return a + b + c;
    }

    public static void main(String[] args) {
        Bar a = new Bar();
        System.out.println(a.inc());
    }

    public int inc () {
        int x ;
        try {
            x = 1;
            return x;
        } catch (Exception e) {
            x = 2;
            return x;
        } finally {
            x = 3;
        }
    }

    class InnerClass {
        int a = 1;
        public void test () {

        }
    }
}
