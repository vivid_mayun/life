package learn.note.JDKCode.JVM;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Wang WenLei
 * @Date 2022/8/13 11:53
 * @Version 1.0
 **/
public class HeapOOM {
    static class OOMObject {

    }

    public static void main(String[] args) {
        List<OOMObject> list = new ArrayList<>();
        while (true) {
            list.add(new OOMObject());
        }
    }
}
