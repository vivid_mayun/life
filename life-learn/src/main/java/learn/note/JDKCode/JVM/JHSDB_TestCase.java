package learn.note.JDKCode.JVM;

import org.omg.CORBA.ObjectHolder;

/**
 * statcObj、instanceObj\localObj存放在哪里?
 * @Author Wang WenLei
 * @Date 2022/8/18 22:00
 * @Version 1.0
 **/
public class JHSDB_TestCase {
    static class Test {
        static ObjectHolder staticObj = new ObjectHolder();
        ObjectHolder instanceObj = new ObjectHolder();
        void foo () {
            ObjectHolder localObj = new ObjectHolder();
            System.out.println("done");// 这里设置断点
        }
    }

    public static void main(String[] args) {
        Test test = new JHSDB_TestCase.Test();
        test.foo();
    }
}
