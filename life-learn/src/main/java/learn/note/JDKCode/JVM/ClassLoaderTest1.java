package learn.note.JDKCode.JVM;

import java.io.IOException;
import java.io.InputStream;

/**
 * 类加载器与instanceof关键字演示
 * @Author Wang WenLei
 * @Date 2022/9/7 22:04
 * @Version 1.0
 **/
public class ClassLoaderTest1 {
    public static void main(String[] args) throws Exception {
        ClassLoader myLoader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                String fileName = name.substring(name.lastIndexOf(".") + 1) + ".class";
                try (InputStream is = getClass().getResourceAsStream(fileName)){
                    if (is == null) {
                        return super.loadClass(name);
                    }
                    try {
                        byte [] b = new byte[is.available()];
                        is.read(b);
                        return defineClass(name,b,0,b.length);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

            }
        };

        Object obj = myLoader.loadClass("learn.note.JDKCode.JVM.ClassLoaderTest1").newInstance();
        System.out.println(obj.getClass());
        System.out.println(ClassLoaderTest1.class);
        System.out.println(obj instanceof learn.note.JDKCode.JVM.ClassLoaderTest1);
    }
}
