package learn.note.redis;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

/**
 * @Author Wang WenLei
 * @Date 2022/5/23 11:25
 * @Version 1.0
 **/
public class BloomFilter {
    /**
     * hash函数执行次数
     */
    private int k;
    /**
     * 位图的大小
     */
    private int m;

    /**
     * @param n 样本容量n
     * @param p 容错率
     */
    BloomFilter(long n, double p) {
        // 用计算求得k和m
        this.m = getM(n,p);
        this.k = getK(n);
    }

    /**
     * 得到位图大小
     * @param n 样本容量
     * @param p 容错率
     * @return 位图大小
     */
    private int getM(long n, double p) {
        return (int) ((- n * Math.log(p)) / (Math.log(2) * Math.log(2)));
    }

    /**
     * @param n 样本容量
     * @return Hash函数执行次数
     */
    private int getK(long n) {
        return Math.max(1, (int) Math.round((double) m / n * Math.log(2)));
    }

    public long [] getBitMapIndex (String value) {
        long [] bitMapIndex = new long[this.k];

        long hash64 = Hashing.murmur3_128().hashString(value, Charsets.UTF_8).asLong();
        int hash = (int) hash64;
        int hash1 = (int) hash64;
        int hash2 = (int) (hash64 >>> 32);

        // 循环k次哈希函数
        for (int i = 0 ; i < k ; i++) {
            bitMapIndex[i] = (hash & Long.MAX_VALUE) % this.m;
            if (i % 2 == 0) {
                hash += hash2;
            } else {
                hash += hash1;
            }
        }
        return bitMapIndex;
    }
}
