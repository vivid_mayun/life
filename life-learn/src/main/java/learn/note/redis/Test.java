package learn.note.redis;

/**
 * @Author Wang WenLei
 * @Date 2022/1/22 11:46
 * @Version 1.0
 **/
public class Test {
    public static void main(String[] args) {
        BloomFilter bf = new BloomFilter(10,0.1);
        long[] tests = bf.getBitMapIndex("test");
        System.out.println(tests);
    }
}
