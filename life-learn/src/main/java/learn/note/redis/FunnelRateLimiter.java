package learn.note.redis;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Wang WenLei
 * @Date 2022/5/26 8:21
 * @Version 1.0
 **/
public class FunnelRateLimiter {
    static class Funnel {
        /**
         * 漏斗容量
         */
        int capacity;
        /**
         * 漏嘴流水速率
         */
        float leakingRate;
        /**
         * 漏斗剩余空间
         */
        int leftQuota;
        /**
         * 上次漏水时间
         */
        long leakingTs;

        public Funnel(int capacity, float leakingRate) {
            this.capacity = capacity;
            this.leakingRate = leakingRate;
            this.leftQuota = capacity;
            this.leakingTs = System.currentTimeMillis();
        }

        void makeSpace() {
            long nowTs = System.currentTimeMillis();
            // 距离上次漏水过去了多久
            long deltaTs = nowTs - leakingTs;
            // 可以腾出的空间
            int deltaQuota = (int) (deltaTs * leakingRate);
            // 间隔时间太长，整数数字过大溢出
            if (deltaQuota < 0) {
                // 漏斗空间变满
                this.leftQuota = capacity;
                // 改为当前时间
                this.leakingTs = nowTs;
                return;
            }
            // 腾出空间太小，最小单位是1。那就下次这次无操作
            if (deltaQuota < 1) {
                return;
            }
            // 漏斗空间增加释放掉的空间
            this.leftQuota += deltaQuota;
            // 改为当前时间
            this.leakingTs = nowTs;
            if (this.leftQuota > this.capacity) {
                // 如果漏斗剩余空间大于总空间，把漏斗剩余空间变满
                this.leftQuota = this.capacity;
            }
        }

        boolean watering(int quota) {
            makeSpace();
            if (this.leftQuota >= quota) {
                this.leftQuota -= quota;
                return true;
            }
            return false;
        }

        boolean watering() {
            return watering(1);
        }
    }

    private Map<String, Funnel> funnels = new HashMap<>();

    public boolean isActionAllowed(String userId, String actionKey, int capacity, float leakingRate) {
        String key = String.format("%s:%s", userId, actionKey);
        Funnel funnel = funnels.get(key);
        if (funnel == null) {
            funnel = new Funnel(capacity,leakingRate);
            funnels.put(key, funnel);
        }
        return funnel.watering();
    }

}
