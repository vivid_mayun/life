package learn.note.algorithm.arr;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。
 * 请你合并所有重叠的区间，并返回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。
 * @author Wang WenLei
 * @version 1.0
 * @since 2023/11/10 13:51
 */
public class MergeArr {
    public static void main(String[] args) {
        int[][] merge = merge(new int[][]{{1, 2}, {1,2}});
        if (merge != null) {
            for (int[] ints : merge) {
                System.out.println(ints[0] + "," + ints[1]);
            }
        }
    }
    public static int[][] merge(int[][] intervals) {
        if (intervals == null || intervals.length == 0) {
            return null;
        }
        if (intervals.length == 1) {
            return intervals;
        }
        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));
        int[][] arr = new int[intervals.length][2];
        int index = 0;
        arr[index] = intervals[0];

        for (int i = 1 ; i < intervals.length ; i++) {
            int[] ints = intervals[i];
            if (ints[0] < arr[index][1]) {
                arr[index][1] = Math.max(arr[index][1], ints[1]);
            } else {
                index++;
                arr[index] = ints;
            }
        }
        return Arrays.copyOfRange(arr, 0,index + 1);
    }
}
