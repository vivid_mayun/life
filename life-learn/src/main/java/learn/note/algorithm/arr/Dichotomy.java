package learn.note.algorithm.arr;

/**
 * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
 * @author Wang WenLei
 * @version 1.0
 * @since 2023/11/9 9:17
 */
public class Dichotomy {
    public static void main(String[] args) {
        int i = searchInsert(new int[]{1, 3, 5, 6}, 5);
        System.out.println(i);
    }

    public static int searchInsert(int[] nums, int target) {
        if (nums == null) {
            return 0;
        }
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            int index = left + ((right - left) >> 1);
            if (nums[index] == target) {
                return index;
            } else if (nums[index] > target) {
                right = index - 1;
            } else {
                left = index + 1;
            }
        }
        return left;
    }
}
