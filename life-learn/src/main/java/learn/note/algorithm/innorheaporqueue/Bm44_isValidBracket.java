package learn.note.algorithm.innorheaporqueue;

import java.util.Stack;

/**
 * 有效括号序列
 * @author WangWenLei
 * @DATE: 2022/3/24
 **/
public class Bm44_isValidBracket {
    public static void main(String[] args) {
        boolean valid = isValid("}}}");
        System.out.println(valid);
    }

    public static boolean isValid (String s) {
        if(s == null){
            return false;
        }
        Stack<Character> temp = new Stack<>();
        for(char item :s.toCharArray()){
            if(item == '['){
                temp.push(']');
            }else if(item == '{'){
                temp.push('}');
            }else if(item == '('){
                temp.push(')');
            }else if(temp.isEmpty() || temp.pop() != item){
                //如果 还有数据 并且不是 [ { (  ,那么temp就是空的，不符合要求，或者弹出的元素不等于当前的 也不是
                return false;
            }
        }
        return temp.isEmpty();
    }
}
