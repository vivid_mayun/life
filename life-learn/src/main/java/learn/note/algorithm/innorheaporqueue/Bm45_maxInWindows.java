package learn.note.algorithm.innorheaporqueue;

import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * @author WangWenLei
 * @DATE: 2022/3/24
 **/
public class Bm45_maxInWindows {
    public static void main(String[] args) {
        maxInWindows(new int[]{2,3,4,2,6,2,5,1},3);
    }

    public static ArrayList<Integer> maxInWindows(int [] num, int size) {
        ArrayList<Integer> list = new ArrayList<>();
        if (num == null || num.length == 0) {
            return list;
        }
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>((o1, o2) -> o2 - o1);
        for (int i = 0 ; i < size ; i++) {
            priorityQueue.add(num[i]);
        }
        list.add(priorityQueue.poll());
        if (num.length < size) {

            return list;
        } else {
            for (int i = 1 ; i < num.length - size + 1 ; i++) {
                PriorityQueue<Integer> single = new PriorityQueue<>((o1, o2) -> o2 - o1);
                for (int j = 0 ; j < size ; j++) {
                    single.add(num[i + j]);
                }
                list.add(single.poll());
            }
        }
        return list;
    }
}
