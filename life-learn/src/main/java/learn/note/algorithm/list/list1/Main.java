package learn.note.algorithm.list.list1;

import java.util.Stack;

/**
 * @Author Wang WenLei
 * @Date 2022/3/1 22:34
 * @Version 1.0
 **/
public class Main {
    private static ListNode createList() {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);

        node1.next = node2;
        node2.next = node3;
        return node1;
    }

    public static void main(String[] args) {
        ListNode head = createList();
//        stack(head);
        ListNode listNode = O1(head);
        System.out.println(listNode);
    }

    private static ListNode O1(ListNode head) {
        if (head == null) return null;
        ListNode cur = head;
        ListNode pre = null;
        while (cur != null) {
            ListNode next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = next;
        }
        return pre;
    }

    public static ListNode stack(ListNode head) {
        ListNode next = head;
        Stack<ListNode> stack = new Stack<>();
        while (next != null) {
            stack.push(next);
            next = next.next;
        }
        ListNode overHead = null;
        if (!stack.empty()) {
            overHead = stack.pop();
            ListNode overNext = overHead;
            while (!stack.empty()) {
                overNext.next = stack.pop();
                overNext = overNext.next;
            }
            overNext.next = null;
        }
        return overHead;
    }


    public static class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }
}
