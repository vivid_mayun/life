package learn.note.algorithm.list;
import java.util.*;
/**
 * @author: Wang WenLei
 * @create: 2022-06-13 18:57
 **/
public class SortInList {
    /**
     *
     * @param head ListNode类 the head node
     * @return ListNode类
     */
    public static ListNode sortInList (ListNode head) {
        if (head == null) {
            return null;
        }

        if (head.next == null) {
            return head;
        }
        // 快慢指针求中点，快指针走完，慢指针就是中点
        ListNode left = head;
        ListNode mid = head.next;
        ListNode right = head.next.next;
        //右边的指针到达末尾时，中间的指针指向该段链表的中间
        while(right != null && right.next != null){
            left = left.next;
            mid = mid.next;
            right = right.next.next;
        }
        ListNode head2 = sortInList(mid);
        left.next = null;
        ListNode head1 = sortInList(head);

        return marge(head1,head2);
    }

    public static ListNode marge(ListNode head1,ListNode head2) {
        if (head1 == null && head2 == null) {
            return null;
        }
        if (head1 == null) {
            return head2;
        }
        if (head2 == null) {
            return head1;
        }
        ListNode next1 = head1;
        ListNode next2 = head2;
        ListNode head = new ListNode(-1);
        ListNode next = head;
        while (next1 != null && next2 != null) {
            if (next1.val <= next2.val) {
                next.next = next1;
                next = next.next;
                next1 = next1.next;
            } else {
                next.next = next2;
                next = next.next;
                next2 = next2.next;
            }
        }
        while (next1 != null) {
            next.next = next1;
            next = next.next;
            next1 = next1.next;
        }
        while (next2 != null) {
            next.next = next2;
            next = next.next;
            next2 = next2.next;
        }
        return head.next;
    }

    public static void main(String[] args) {
        ListNode head1 = new ListNode(1);
        ListNode head2 = new ListNode(3);
        ListNode head3 = new ListNode(2);
        ListNode head4 = new ListNode(4);
        ListNode head5 = new ListNode(5);

        head1.next = head2;
        head2.next = head3;
        head3.next = head4;
        head4.next =head5;

        ListNode listNode = sortInList(head1);
        System.out.println(listNode);
    }
}
