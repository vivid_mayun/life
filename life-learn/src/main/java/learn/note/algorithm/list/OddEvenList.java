package learn.note.algorithm.list;

/**
 * @Author Wang WenLei
 * @Date 2022/6/16 21:24
 * @Version 1.0
 **/
public class OddEvenList {
    public static ListNode oddEvenList (ListNode head) {
        if (head == null) {
            return null;
        }
        if (head.next == null || head.next.next == null) {
            return null;
        }
        ListNode ji = new ListNode(-1);
        ListNode jiNext = ji;
        ListNode ou = new ListNode(-2);
        ListNode ouNext = ou;
        int i = 1;
        ListNode next = head;
        while (next != null) {
            if ((i % 2) == 0) {
                ouNext.next = next;
                ouNext = ouNext.next;
            } else {
                jiNext.next = next;
                jiNext = jiNext.next;
            }
            i++;
            next = next.next;
        }
        jiNext.next = ou.next;
        return ji.next;
    }

    public static void main(String[] args) {
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(1);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(5);
        ListNode node5 = new ListNode(6);
        ListNode node6 = new ListNode(4);
        ListNode node7 = new ListNode(7);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        node5.next = node6;
        node6.next = node7;
        ListNode listNode = oddEvenList(node1);
        System.out.println(listNode);
    }
}
