package learn.note.algorithm.list;

import java.util.Stack;

/**
 * @author: Wang WenLei
 * @create: 2022-06-13 15:50
 **/
public class AddInList {
    public static ListNode addInList (ListNode head1, ListNode head2) {
        if (head1 == null && head2 == null) {
            return null;
        }
        if (head1 == null) {
            return head2;
        }
        if (head2 == null) {
            return head1;
        }
        Stack<ListNode> stack1 = new Stack<>();
        ListNode next = head1;
        while (next != null) {
            stack1.push(next);
            next = next.next;
        }

        Stack<ListNode> stack2 = new Stack<>();
        next = head2;
        while (next != null) {
            stack2.push(next);
            next = next.next;
        }
        ListNode head = new ListNode(-1);
        next = head;
        ListNode curNode = null;
        int pro = 0;
        while (!stack1.empty() && !stack2.empty()) {
            curNode = next;
            ListNode h1 = stack1.pop();
            ListNode h2 = stack2.pop();
            int cur = h1.val + h2.val;
            cur += pro;
            pro = cur / 10;
            next.next = new ListNode(cur % 10);
            next = next.next;
        }
        while (!stack1.empty()) {
            curNode = next;
            ListNode h1 = stack1.pop();
            int cur = h1.val + pro;
            pro = cur / 10;
            next.next = new ListNode(cur % 10);
            next = next.next;
        }
        while (!stack2.empty()) {
            curNode = next;
            ListNode h2 = stack2.pop();
            int cur = h2.val + pro;
            pro = cur / 10;
            next.next = new ListNode(cur % 10);
            next = next.next;
        }
        if (pro > 0) {
            next.next = new ListNode(pro);
        }
        head = head.next;

        // 反转
        next = head;
        curNode = null;
        while (next != null) {
            ListNode cur = next.next;
            next.next = curNode;
            curNode = next;
            next = cur;
        }
        return curNode;
    }

    public static void main(String[] args) {
        Stack<ListNode> stack = new Stack<>();
        ListNode head11 = new ListNode(9);
        ListNode head12 = new ListNode(3);
        ListNode head13 = new ListNode(7);
        ListNode head21 = new ListNode(6);
        ListNode head22 = new ListNode(3);
        head11.next = head12;
        head12.next = head13;

        head21.next = head22;
        ListNode listNode = addInList(head11, head21);
        System.out.println(listNode);
    }
}
