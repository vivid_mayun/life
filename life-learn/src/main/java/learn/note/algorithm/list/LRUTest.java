package learn.note.algorithm.list;

/**
 * @Author Wang WenLei
 * @Date 2022/7/25 19:27
 * @Version 1.0
 **/
public class LRUTest {
    static class Node {
        public int i;
        public Node next;

        public Node(int i) {
            this.i = i;
        }
    }


    public static void deletedNode (Node head,int i) {
        if (head != null) {
            Node cur = head;
            while (cur != null && cur.next != null) {
                if (cur.next.i == i) {
                    cur.next = cur.next.next;
                }
                cur = cur.next;
            }
        }
    }

    public static void main(String[] args) {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        deletedNode(node1,3);
        System.out.println();
    }
}
