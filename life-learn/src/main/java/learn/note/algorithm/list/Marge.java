package learn.note.algorithm.list;

import java.util.*;

/**
 * @Author Wang WenLei
 * @Date 2022/6/8 11:38
 * @Version 1.0
 **/
public class Marge {
    static class QueueComp implements Comparator<ListNode> {
        public int compare(ListNode o1,ListNode o2) {
            return o1.val - o2.val;
        }
    }
    public ListNode mergeKLists(ArrayList<ListNode> lists) {
        Queue<ListNode> q = new PriorityQueue<>(new QueueComp());
        for (ListNode item : lists) {
            q.add(item);
        }
        ListNode head = new ListNode(-1);
        ListNode next = head;
        while (q.size() != 0) {
            ListNode cur = q.poll();
            head.next = cur;
            cur = cur.next;
            q.add(cur);
        }
        return head.next;
    }
}
