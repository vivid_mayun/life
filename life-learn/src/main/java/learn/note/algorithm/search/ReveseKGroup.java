package learn.note.algorithm.search;

/**
 * @Author Wang WenLei
 * @Date 2022/6/7 17:33
 * @Version 1.0
 **/
public class ReveseKGroup {
    static class ListNode {
        ListNode next;
        int val;
        ListNode(ListNode next,int val) {
            this.next = next;
            this.val = val;
        }
    }
    public static ListNode reverseKGroup (ListNode head, int k) {
        if (head == null || k <= 1) {
            return head;
        }
        ListNode next = head;
        int sum = 0;
        while (next != null) {
            sum ++;
            next = next.next;
        }
        int i = 1;
        int size = (sum / k) * k;
        ListNode node = null;
        ListNode cheardHead = head;
        next = head;
        while (next != null) {
            if (i < size) {
                if ((i%k) == 0) {
                    // 保存下一个节点
                    ListNode p = next.next ;
                    // 完成当前节点的反转
                    next.next = node;
                    cheardHead.next = p;
                    // 开始一个新的反转
                    cheardHead = p;
                    if (i == k) {
                        head = node;
                    }
                    node = null;
                    // 向下移动
                    next = p;
                }else {
                    ListNode p = next.next ;
                    next.next = node;
                    node = next;
                    // 向下移动
                    next = p;
                }
            } else {
                node.next = next;
                break;
            }
            i++;
        }
        return head;
    }

    public static void main(String[] args) {
        ListNode node1 = new ListNode(null, 1);
        ListNode node2 = new ListNode(null, 2);
        ListNode node3 = new ListNode(null, 3);
        ListNode node4 = new ListNode(null, 4);
        ListNode node5 = new ListNode(null, 5);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        ListNode listNode = reverseKGroup(node1, 2);
        System.out.println(listNode);
    }
}
