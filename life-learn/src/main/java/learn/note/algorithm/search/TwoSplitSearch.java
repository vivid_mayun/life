package learn.note.algorithm.search;

/**
 * @Author Wang WenLei
 * @Date 2022/6/6 22:14
 * @Version 1.0
 **/
public class TwoSplitSearch {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param nums int整型一维数组
     * @param target int整型
     * @return int整型
     */
    public static int search (int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        return search(nums,target,0,nums.length -1);
    }

    public static int search (int[] nums, int target,int s ,int e) {
        if (e <= s) {
            return nums [s] == target ? s : -1;
        }
        int tip = (s + e) / 2;
        if (nums[tip] > target) {
            return search(nums,target,s,tip - 1);
        } else {
            return search(nums,target,tip + 1,e);
        }
    }
    public static void main(String[] args) {
        int [] nums = new int[] {0,1};
        int target = 1;
        System.out.println(search(nums,target));
    }
}
