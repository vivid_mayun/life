package learn.note.algorithm.sort;

import java.util.Arrays;

/**
 * 冒泡排序
 * @author WangWenLei
 * @DATE: 2021/5/20
 **/
public class BubbleSort {
    public static void main(String[] args) {
        int [] arry = new int[] {5,8,5,2,9,1};
        sort(arry);
        System.out.println(Arrays.toString(arry));
    }

    private static void sort(int [] arr) {
        System.out.println(arr.length);
        for (int i = 0 ; i < arr.length - 1 ; i++) {
            for (int j = 0 ; j < arr.length - 1 ; j ++) {
                if (arr[j] > arr[j+1]) {
                    swap(arr,j,j+1);
                }
            }
        }
    }

    private static void swap(int [] data,int i,int j) {
        int temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }
}
