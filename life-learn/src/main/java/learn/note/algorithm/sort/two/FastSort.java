package learn.note.algorithm.sort.two;

import java.util.Arrays;

/**
 * @author: Wang WenLei
 * @create: 2022-10-24 11:12
 **/
public class FastSort {
    public static void main(String[] args) {
        int [] arry = new int[] {5,8,5,2,9};
        sort(arry);
        System.out.println(Arrays.toString(arry));
    }

    protected static void sort (int [] arr) {
        fastSort(arr,0,arr.length - 1);
    }

    private static void fastSort(int[] arr, int left, int right) {
        if (left < right) {
            int i = left;
            int j = right;
            int temp = arr[left];
            while (i < j) {
                while (arr[j] > temp && i < j) {j--;}
                if (i < j) {
                    arr[i] = arr[j];
                    i++;
                }
                while (arr[i] < temp && i < j) {i++;}
                if (i < j) {
                    arr[j] = arr[i];
                    j--;
                }
            }
            arr[i] = temp;
            fastSort(arr,left,i - 1);
            fastSort(arr,i + 1,right);
        }
    }
}
