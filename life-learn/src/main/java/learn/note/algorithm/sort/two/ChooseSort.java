package learn.note.algorithm.sort.two;

import java.util.Arrays;

/**
 * @author: Wang WenLei
 * @create: 2022-10-12 09:32
 **/
public class ChooseSort {
    public static void main(String[] args) {
        int [] arry = new int[] {5,8,5,2,9};
        chooseSort(arry);
        System.out.println(Arrays.toString(arry));
    }

    private static void chooseSort(int [] arr) {
        for (int i = 0 ; i < arr.length - 1 ; i ++) {
            int minIndex = i;
            for (int j = i + 1 ; j < arr.length ; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            swap(arr,i,minIndex);
        }
    }

    private static void swap(int [] data,int i,int j) {
        int temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }
}
