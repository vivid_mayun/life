package learn.note.algorithm.sort.two;

import java.util.Arrays;

/**
 * @author: Wang WenLei
 * @create: 2022-10-13 16:59
 **/
public class InsertSort {
    public static void main(String[] args) {
        int [] arry = new int[] {5,8,5,2,9};
        sort(arry);
        System.out.println(Arrays.toString(arry));
    }

    private static void sort (int [] arr) {
        for (int i = 0 ; i < arr.length ; i ++) {
            for (int j = i ; j > 0 ; j--) {
                if (arr[j] < arr[j - 1]) {
                    swap(arr,j,j - 1);
                }
            }
        }
    }

    private static void swap(int [] arr ,int i,int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
