package learn.note.algorithm.sort.two;

import java.util.Arrays;

/**
 * @author: Wang WenLei
 * @create: 2022-10-24 11:05
 **/
public class MergeSort {
    public static void main(String[] args) {
        int [] arry = new int[] {3,5,2,7,9,1,4,6,0};
        sort(arry);
        System.out.println(Arrays.toString(arry));
    }

    protected static void sort(int [] arr) {
        int [] tempArr = new int[arr.length];
        mergeSort(arr,tempArr,0,arr.length - 1);
    }

    private static void mergeSort(int[] arr, int[] tempArr, int left, int right) {
        if (left < right) {
            int cut = (left + right) >> 1;
            mergeSort(arr,tempArr,left,cut);
            mergeSort(arr,tempArr,cut + 1,right);
            merge(arr,tempArr,left,cut,right);
        }
    }

    private static void merge(int[] arr, int[] tempArr, int left, int cut, int right) {
        int i = left;
        int j = cut + 1;
        int pos = left;
        while (i <= cut && j <= right) {
            if (arr[i] < arr[j]) {
                tempArr[pos++] = arr[i++];
            } else {
                tempArr[pos++] = arr[j++];
            }
        }
        while (i <= cut) {
            tempArr[pos++] = arr[i++];
        }
        while (j <= right) {
            tempArr[pos++] = arr[j++];
        }
        while (left <= right) {
            arr[left] = tempArr[left];
            left++;
        }
    }
}
