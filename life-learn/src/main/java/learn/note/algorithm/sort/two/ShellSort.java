package learn.note.algorithm.sort.two;

import java.util.Arrays;

/**
 * @author: Wang WenLei
 * @create: 2022-10-13 17:12
 **/
public class ShellSort {
    public static void main(String[] args) {
        int [] arry = new int[] {3,5,2,7,9,1,4,6,0};
        sort(arry);
        System.out.println(Arrays.toString(arry));
    }

    public static void sort(int [] arr) {
        int gap = arr.length;
        while(gap > 1) {
            gap /= 2;
            for (int i = 0 ; i < arr.length ; i+=gap) {
                for (int j = i; j > 0 ; j-=gap) {
                    if (arr[j] < arr[j-gap]) {
                        swap(arr,j,j-gap);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    private static void swap(int [] data,int i,int j) {
        int temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }
}
