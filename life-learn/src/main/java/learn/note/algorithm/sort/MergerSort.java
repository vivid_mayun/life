package learn.note.algorithm.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author WangWenLei
 * @DATE: 2021/9/6
 **/
public class MergerSort {
    public static void main(String[] args) {
        int [] arr = new int[] {2,3,6,4,3,8,14,72,13,854,237,12,6,8};
        System.out.println(Arrays.toString(arr));
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void sort (int [] arr) {
        int [] tempArr = new int[arr.length];
        margeSort(arr,tempArr,0,arr.length - 1);
    }

    private static void margeSort(int[] arr, int[] tempArr, int left, int right) {
        if (left >= right) {
            return;
        }
        int cut = (left + right) >> 1;
        margeSort(arr,tempArr,left,cut);
        margeSort(arr,tempArr,cut + 1,right);
        marge(arr,tempArr,left,right,cut);
    }

    private static void marge(int[] arr, int[] tempArr, int left, int right, int cut) {
        int i = left;
        int j = cut + 1;
        int pos = left;
        while (i <= cut && j <= right) {
            if (arr[i] < arr[j]) {
                tempArr[pos++] = arr[i++];
            } else {
                tempArr[pos++] = arr[j++];
            }
        }
        while(i <= cut) {
            tempArr[pos++] = arr[i++];
        }
        while(j <= right) {
            tempArr[pos++] = arr[j++];
        }
        while (left <= right) {
            arr[left] = tempArr[left];
            left++;
        }
    }
}
