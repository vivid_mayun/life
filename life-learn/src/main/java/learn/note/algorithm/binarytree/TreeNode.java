package learn.note.algorithm.binarytree;

/**
 * @author WangWenLei
 * @DATE: 2022/3/9
 **/
public class TreeNode {
    public int val = 0;
    public TreeNode left = null;
    public TreeNode right = null;

    public TreeNode(int val) {
      this.val = val;
    }
}
