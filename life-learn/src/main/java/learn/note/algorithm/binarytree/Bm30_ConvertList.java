package learn.note.algorithm.binarytree;

import java.util.Stack;

/**
 * @Author Wang WenLei
 * @Date 2022/3/13 11:38
 * @Version 1.0
 **/
public class Bm30_ConvertList {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();

        TreeNode convert = convert(tree);
        System.out.println(convert);
    }

    public static TreeNode convert(TreeNode pRootOfTree) {
        if (pRootOfTree == null) {
            return null;
        }
        Stack<TreeNode> stack = new Stack<>();

        TreeNode pre = null;
        TreeNode cur = pRootOfTree;
        TreeNode head = null;

        while (!stack.empty() || cur != null) {
            if (cur != null) {
                stack.push(cur);
                cur = cur.left;
            } else {
                cur = stack.pop();

                if (pre != null) {
                    pre.right = cur;
                    cur.left = pre;
                } else {
                    head = cur;
                    cur.left = null;
                }
                pre = cur;
                cur = cur.right;
            }
        }

        return head;
    }
}
