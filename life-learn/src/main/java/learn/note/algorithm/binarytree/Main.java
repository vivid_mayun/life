package learn.note.algorithm.binarytree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @author WangWenLei
 * @DATE: 2022/3/9
 **/
public class Main {
    public static TreeNode createTree () {
        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);
        TreeNode node6 = new TreeNode(6);
        TreeNode node7 = new TreeNode(7);
        TreeNode node8 = new TreeNode(8);

        node1.left = node2;
        node1.right = node3;
        node2.left = node4;
        node2.right = node5;
        node3.left = node6;
        node3.right = node7;
        node4.left = node8;

        return node1;
    }

    public static void main(String[] args) {
        TreeNode root = createTree();
//        int[] ints = preorderTraversal(root);
        int[] ints = postorderTraversalNotRecursion(root);
        for ( int i = 0 ; i < ints.length ; i++) {
            if (i == (ints.length - 1)) {
                System.out.println(ints[i]);
            } else {
                System.out.print(ints[i] + ",");
            }
        }
    }

    /**
     * 前序遍历-递归
     * @param root 根节点
     * @return 所有节点的数组
     */
    public static int[] preorderTraversal (TreeNode root) {
        List<TreeNode> list = new ArrayList<>();
        preorderTraversal(root,list);
        int [] data = new int [list.size()];
        for (int i = 0 ; i < list.size() ; i++) {
            data[i] = list.get(i).val;
        }
        return data;
    }

    /**
     * 前序遍历-非递归
     * @param root 根节点
     * @return 所有节点的数组
     */
    public static int[] preorderTraversalNoRecursion (TreeNode root) {
        List<TreeNode> list = new ArrayList<>();
        if (root == null) {
            return new int[]{};
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.empty()) {
            TreeNode node = stack.pop();
            list.add(node);
            if (node.right != null) {
                stack.push(node.right);
            }
            if (node.left != null) {
                stack.push(node.left);
            }
        }
        int [] data = new int [list.size()];
        for (int i = 0 ; i < list.size() ; i++) {
            data[i] = list.get(i).val;
        }
        return data;
    }

    private static int getNodeNum(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        int left = getNodeNum(root.left);
        int right = getNodeNum(root.right);
        return left + right + 1;
    }

    /**
     * 中序遍历-递归
     * @param root 根节点
     * @return 所有节点的数组
     */
    public static int[] inorderTraversal (TreeNode root) {
        List<TreeNode> list = new ArrayList<>();
        inorderTraversal(root,list);
        int [] data = new int[list.size()];
        for (int i = 0 ; i < list.size() ; i++) {
            data[i] = list.get(i).val;
        }
        return data;
    }

    /**
     * 中序遍历-非递归
     * @param root 根节点
     * @return 所有节点的数组
     */
    public static int[] inorderTraversalNotRecursion (TreeNode root) {
        List<TreeNode> list = new ArrayList<>();
        if (root == null) {
            return new int[] {};
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while (!stack.empty() || cur != null) {
            if (cur != null) {
                stack.push(cur);
                cur = cur.left;
            } else {
                cur = stack.pop();
                // 弹出就打印
                list.add(cur);
                // 结点移动到右结点上去周而复始，让右树重复操作
                cur = cur.right;
            }
        }
        int [] data = new int[list.size()];
        for (int i = 0 ; i < list.size() ; i++) {
            data[i] = list.get(i).val;
        }
        return data;
    }

    /**
     * 后序遍历-递归
     * @param root 根节点
     * @return 所有节点的数组
     */
    public static int[] postorderTraversal (TreeNode root) {
        List<TreeNode> list = new ArrayList<>();
        postorderTraversal(root,list);
        int [] data = new int[list.size()];
        for (int i = 0 ; i < list.size() ;i ++) {
            data[i] = list.get(i).val;
        }
        return data;
    }

    /**
     * 后序遍历-非递归
     * @param root 根节点
     * @return 所有节点的数组
     */
    public static int[] postorderTraversalNotRecursion (TreeNode root) {
        if (root == null) {
            return new int[]{};
        }
        List<TreeNode> list = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        Stack<TreeNode> re = new Stack<>();
        stack.push(root);
        while (!stack.empty()) {
            TreeNode cur = stack.pop();
            re.push(cur);
            if (cur.right != null) {
                stack.push(cur.right);
            }
            if (cur.left != null) {
                stack.push(cur.left);
            }
        }
        while (!re.empty()) {
            list.add(re.pop());
        }
        int [] data = new int[list.size()];
        for (int i = 0 ; i < list.size() ;i ++) {
            data[i] = list.get(i).val;
        }
        return data;
    }

    public static void preorderTraversal(TreeNode root, List<TreeNode> list) {
        if (root == null) {
            return;
        }
        list.add(root);
        preorderTraversal(root.left,list);
        preorderTraversal(root.right,list);
    }

    public static void inorderTraversal (TreeNode root, List<TreeNode> list) {
        if (root == null) {
            return;
        }
        inorderTraversal(root.left,list);
        list.add(root);
        inorderTraversal(root.right,list);
    }

    public static void postorderTraversal (TreeNode root,List<TreeNode> list) {
        if (root == null) {
            return;
        }
        postorderTraversal(root.left,list);
        postorderTraversal(root.right,list);
        list.add(root);
    }
}
