package learn.note.algorithm.binarytree;

/**
 * 二叉搜索树的最近公共祖先
 * @author WangWenLei
 * @DATE: 2022/3/21
 **/
public class Bm37_LowestCommonAncestor {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();
        System.out.println();
    }

    public int lowestCommonAncestor (TreeNode root, int p, int q) {
        return lowestCommon(root,p,q).val;
    }

    public TreeNode lowestCommon (TreeNode root, int p, int q) {
        if (root == null) {
            return new TreeNode(0);
        }
        if (root.val > p && root.val > q) {
            return lowestCommon (root.left, p, q) ;
        } else if (root.val < p && root.val < q) {
            return lowestCommon (root.right, p, q) ;
        } else {
            return root;
        }
    }
}
