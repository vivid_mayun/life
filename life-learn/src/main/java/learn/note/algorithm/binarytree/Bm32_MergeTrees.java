package learn.note.algorithm.binarytree;

/**
 * @author WangWenLei
 * @DATE: 2022/3/17
 **/
public class Bm32_MergeTrees {
    public static void main(String[] args) {

    }

    public TreeNode mergeTrees (TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) {
            return null;
        } else if (t1 == null) {
            return t2;
        } else if (t2 == null) {
            return t1;
        } else {
            t1.val = t1.val + t2.val;
            TreeNode n1 = mergeTrees(t1.left,t2.left);
            TreeNode n2 = mergeTrees(t1.right,t2.right);
            if (n1 == null && n2 == null) {
                t1.left = null;
                t1.right = null;
            } else if (n1 == null) {
                t1.right = n2;
            } else if (n2 == null) {
                t1.left = n1;
            } else {
                t1.left = n1;
                t1.right = n2;
            }
            return t1;
        }
    }
}
