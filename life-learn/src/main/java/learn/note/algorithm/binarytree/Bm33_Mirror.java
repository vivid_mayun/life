package learn.note.algorithm.binarytree;

/**
 * 二叉树的镜像
 * @Author Wang WenLei
 * @Date 2022/3/17 22:21
 * @Version 1.0
 **/
public class Bm33_Mirror {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();
        TreeNode mirror = mirror(tree);
    }

    /**
     * 递归写法
     * @param pRoot 跟结点
     * @return 反转后结点
     */
    public static TreeNode mirror (TreeNode pRoot) {
        if (pRoot == null) {
            return null;
        }

        mirror(pRoot.left);
        mirror(pRoot.right);
        TreeNode left = pRoot.left;
        pRoot.left = pRoot.right;
        pRoot.right = left;

        return pRoot;
    }
}
