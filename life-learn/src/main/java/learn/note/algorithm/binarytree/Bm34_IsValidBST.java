package learn.note.algorithm.binarytree;

/**
 * @Author Wang WenLei
 * @Date 2022/3/17 23:10
 * @Version 1.0
 **/
public class Bm34_IsValidBST {

    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();
        boolean validBST = isValidBST(new TreeNode(2));

    }

    public static boolean isValidBST (TreeNode root) {
        Node resport = resport(root);
        return resport.isBST;
    }

    public static Node resport (TreeNode root) {
        if (root == null) {
            return new Node(Integer.MAX_VALUE,Integer.MIN_VALUE,true);
        }

        Node left = resport(root.left);
        Node right = resport(root.right);
        boolean isBst = (left.max < root.val) && (right.min > root.val);
        return new Node(Math.min(Math.min(root.val,left.min),right.min),Math.max(Math.max(root.val,left.max),right.max),(left.isBST && right.isBST && isBst));
    }

    public static class Node {
        Integer min;
        Integer max;
        boolean isBST;

        public Node(Integer min,Integer max,boolean isBST) {
            this.min = min;
            this.max = max;
            this.isBST = isBST;
        }
    }
}
