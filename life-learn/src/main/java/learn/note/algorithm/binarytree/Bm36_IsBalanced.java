package learn.note.algorithm.binarytree;

/**
 * 是否是平衡二叉树
 * @Author Wang WenLei
 * @Date 2022/3/20 10:35
 * @Version 1.0
 **/
public class Bm36_IsBalanced {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();
        IsBalanced_Solution(tree);
    }

    public static boolean IsBalanced_Solution(TreeNode root) {
        return isBalanced(root).isBalanced;
    }

    public static IsBalanced isBalanced(TreeNode root) {
        if (root == null) {
            return new IsBalanced(true,0);
        }

        IsBalanced left = isBalanced(root.left);
        IsBalanced right = isBalanced(root.right);
        boolean is = left.isBalanced && right.isBalanced && (Math.abs(left.height - right.height) <= 1);
        int height = Math.max(left.height,right.height) + 1;
        return new IsBalanced(is,height);
    }

    public static class IsBalanced {
        public boolean isBalanced;
        public int height;
        public IsBalanced(boolean isBalanced, int height) {
            this.height = height;
            this.isBalanced = isBalanced;
        }
    }
}
