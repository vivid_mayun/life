package learn.note.algorithm.binarytree;

/**
 * @Author Wang WenLei
 * @Date 2022/3/12 11:01
 * @Version 1.0
 **/
public class Bm29_HasPathSum {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();

        boolean b = hasPathSum(tree, 1);
        System.out.println(b);
    }

    public static boolean hasPathSum (TreeNode root, int sum) {
        return hasPathSum(root,sum,0);
    }

    public static boolean hasPathSum (TreeNode root, int sum, int add) {
        if (root == null) {
            return false;
        }
        add += root.val;
        boolean b = hasPathSum(root.left, sum,add);
        if (b) {
            return true;
        }

        if (add == sum && root.left == null && root.right == null) {
            return true;
        }
        if (add < sum && root.left == null && root.right == null) {
            add -= root.val;
        }
        boolean c = hasPathSum(root.right,sum,add);
        if (c) {
            return true;
        }
        return false;
    }
}
