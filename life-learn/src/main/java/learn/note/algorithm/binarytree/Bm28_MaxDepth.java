package learn.note.algorithm.binarytree;

/**
 * 求给定二叉树的最大深度，
 * @author WangWenLei
 * @DATE: 2022/3/11
 **/
public class Bm28_MaxDepth {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();
        System.out.println(maxDepth(tree));
    }

    public static int maxDepth (TreeNode root) {
        if (root == null) {
            return 0;
        }

        int left = maxDepth(root.left);
        int right = maxDepth(root.right);
        return Math.max(left,right) + 1;
    }
//
//    public static int maxDepthNotRecursion (TreeNode root) {
//        if (root == null) {
//            return 0;
//        }
//        int max = Integer.MIN_VALUE;
//
//    }
}
