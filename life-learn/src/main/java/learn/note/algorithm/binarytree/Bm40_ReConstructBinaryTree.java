package learn.note.algorithm.binarytree;

import java.util.Arrays;

/**
 * 重建二叉树
 * @author WangWenLei
 * @DATE: 2022/3/22
 **/
public class Bm40_ReConstructBinaryTree {
    public static void main(String[] args) {

    }

    public TreeNode reConstructBinaryTree(int [] pre,int [] vin) {
        if (pre == null || pre.length == 0) {
            return  null;
        }
        if (vin == null || vin.length == 0) {
            return  null;
        }
        TreeNode root = new TreeNode(pre[0]);
        for (int i = 0 ; i < vin.length ; i++) {
            if (vin[i] == pre[0]) {
                root.left = reConstructBinaryTree(Arrays.copyOfRange(pre,1,i + 1),Arrays.copyOfRange(vin,0,i));
                root.right = reConstructBinaryTree(Arrays.copyOfRange(pre,i + 1,pre.length),Arrays.copyOfRange(vin,i + 1,vin.length));
                break;
            }
        }
        return root;
    }

}
