package learn.note.algorithm.binarytree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 是否是完全二叉树
 * @author WangWenLei
 * @DATE: 2022/3/18
 **/
public class Bm35_IsCompleteTree {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();
        boolean completeTree = isCompleteTree(tree);
    }

    public static boolean isCompleteTree (TreeNode root) {
        if (root == null) {
            return true;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        boolean isLeaf = false;
        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            if (poll == null && !queue.isEmpty()) {
                return false;
            } else if (poll == null) {
                return true;
            }
            if (poll.left != null && poll.right != null) {
                queue.add(poll.left);
                queue.add(poll.right);
            } else if (poll.left != null) {
                queue.add(poll.left);
                queue.add(null);
            } else if (poll.right != null){
                return false;
            } else {
                isLeaf = true;
            }
            boolean hasNode = poll.left != null || poll.right != null;
            if (isLeaf && hasNode) {
                return false;
            }
        }
        return true;
    }
}
