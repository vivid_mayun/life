package learn.note.algorithm.binarytree;

import java.util.*;

/**
 * 蛇形打印遍历二叉树
 * @author WangWenLei
 * @DATE: 2022/3/11
 **/
public class Bm27_Print {
    public static TreeNode createTree () {
        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);
        TreeNode node6 = new TreeNode(6);
        TreeNode node7 = new TreeNode(7);
        TreeNode node8 = new TreeNode(8);
        TreeNode node9 = new TreeNode(9);

        node1.left = node2;
        node1.right = node3;
        node2.left = node4;
        node2.right = node5;
        node3.left = node6;
        node3.right = node7;
        node4.left = node8;
        node4.right = node9;

        return node1;
    }

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> print = print(createTree());
        System.out.println(print);
    }

    public static ArrayList<ArrayList<Integer>> print(TreeNode pRoot) {
        if (pRoot == null) {
            return new ArrayList<>();
        }

        ArrayList<ArrayList<Integer>> lists = new ArrayList<>();
        ArrayList<Integer> list = new ArrayList<>();
        Stack<TreeNode> s1 = new Stack<>();
        Stack<TreeNode> s2 = new Stack<>();
        s1.push(pRoot);

        while (!s1.isEmpty() || !s2.isEmpty()) {
            while (!s1.isEmpty()) {
                TreeNode pop = s1.pop();
                list.add(pop.val);
                if (pop.left != null) {
                    s2.add(pop.left);
                }
                if (pop.right != null) {
                    s2.add(pop.right);
                }
            }
            if (!list.isEmpty()) {
                lists.add(list);
                list = new ArrayList<>();
            }

            while (!s2.isEmpty()) {
                TreeNode pop = s2.pop();
                list.add(pop.val);
                if (pop.right != null) {
                    s1.add(pop.right);
                }
                if (pop.left != null) {
                    s1.add(pop.left);
                }

            }
            if (!list.isEmpty()) {
                lists.add(list);
                list = new ArrayList<>();
            }
        }

        return lists;
    }

}
