package learn.note.algorithm.binarytree;

import java.util.*;

/**
 * 对称二叉树
 * @author WangWenLei
 * @DATE: 2022/3/17
 **/
public class Bm31_IsSymmetrical {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();
        boolean symmetrical = isSymmetrical(tree);

    }

    public static boolean isSymmetrical(TreeNode pRoot) {
        if (pRoot  == null) {
            return true;
        }
        Queue<Integer> queue = new LinkedList<>();
        isSymmetrical1(pRoot.left,queue);
        return isSymmetrical2(pRoot.right,queue) && queue.isEmpty();
    }

    public static void isSymmetrical1(TreeNode pRoot, Queue<Integer> list) {
        if (pRoot  == null) {
            list.add(null);
            return;
        }
        list.add(pRoot.val);
        isSymmetrical1(pRoot.left,list);
        isSymmetrical1(pRoot.right,list);
    }
    public static boolean isSymmetrical2(TreeNode pRoot, Queue<Integer> list) {
        if (pRoot  == null) {
            Integer poll = list.poll();
            if (poll != null) {
                return false;
            }
            return true;
        }
        if (list.isEmpty()) {
            return false;
        }
        Integer pop = list.poll();

        if (pop == null || pRoot.val != pop) {
            return false;
        }
        boolean symmetrical2 = isSymmetrical2(pRoot.right, list);
        if (!symmetrical2) {
            return false;
        }
        symmetrical2 = isSymmetrical2(pRoot.left,list);
        if (!symmetrical2) {
            return false;
        }
        return symmetrical2;
    }
}
