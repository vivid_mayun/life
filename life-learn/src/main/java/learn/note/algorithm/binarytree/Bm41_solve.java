package learn.note.algorithm.binarytree;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author WangWenLei
 * @DATE: 2022/3/23
 **/
public class Bm41_solve {
    private static final Map <String,String> map = new ConcurrentHashMap<>(20);
    public static void main(String[] args) {
        map.putIfAbsent("a","a");
    }

    public int[] solve (int[] xianxu, int[] zhongxu) {
        TreeNode tree = re(xianxu, zhongxu);
        if (tree == null) {
            return new int[]{};
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(tree);
        List<Integer> list = new ArrayList<>();
        TreeNode poll =  null;
        while (!queue.isEmpty()) {
            int size = queue.size();
            while (size > 0) {
                size--;
                poll = queue.poll();
                if (poll.left != null) {
                    queue.add(poll.left);
                }
                if (poll.right != null) {
                    queue.add(poll.right);
                }
            }
            list.add(poll.val);
        }
        int [] arr = new int[list.size()];
        for (int i = 0 ; i < list.size() ; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    public TreeNode re(int[] xianxu, int[] zhongxu) {
        if (xianxu == null || xianxu.length == 0) {
            return null;
        }

        if (zhongxu == null || zhongxu.length == 0) {
            return null;
        }

        TreeNode root = new TreeNode(xianxu[0]);

        for (int i = 0 ; i < zhongxu.length ; i++) {
            if (xianxu[0] == zhongxu[i]) {
                root.left = re(Arrays.copyOfRange(xianxu,1,i + 1),Arrays.copyOfRange(zhongxu,0,i));
                root.right = re(Arrays.copyOfRange(xianxu,i + 1,xianxu.length),Arrays.copyOfRange(zhongxu,i + 1,zhongxu.length));
            }
        }
        return root;
    }
}
