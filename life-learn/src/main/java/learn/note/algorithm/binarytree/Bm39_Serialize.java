package learn.note.algorithm.binarytree;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * 序列化二叉树
 * @author WangWenLei
 * @DATE: 2022/3/22
 **/
public class Bm39_Serialize {
    public static void main(String[] args) {
        TreeNode tree = CreateTree.createTree();
        String s = Serialize(tree);
        System.out.println(s );
        TreeNode deserialize = Deserialize(s );
        System.out.println(deserialize);
    }

    public static String Serialize1(TreeNode root) {
        if (root == null) {
            return "#";
        }
        String left = Serialize1(root.left);
        String right = Serialize1(root.right);
        return root.val + "_" + left + "_" + right;
    }

    public static TreeNode Deserialize1(String str) {
        if (str == null || str.isEmpty() || str.trim().isEmpty()) {
            return null;
        }
        if ("#".equals(str)) {
            return null;
        }
        String[] s = str.split("_");
        TreeNode tree  = new TreeNode(Integer.parseInt(s[0]));
        TreeNode cur = tree;
        Stack<TreeNode> stack = new Stack<>();
        stack.add(cur);
        for(int i = 1 ; i < s.length ; i++) {
            if ("#".equals(s[i])) {
                TreeNode pop = null;
                if ("#".equals(s[i - 1])) {
                    pop = stack.pop();
                } else {
                    pop = cur;
                }
                cur = pop.right;
                if (pop.right == null) {
                    cur = pop;
                }
            } else {
                if ("#".equals(s[i - 1])) {
                    cur.right = new TreeNode(Integer.parseInt(s[i]));
                    cur = cur.right;
                } else {
                    cur.left = new TreeNode(Integer.parseInt(s[i]));
                    cur = cur.left;
                }
                stack.add(cur);
            }
        }
        return tree;
    }

    public static String Serialize(TreeNode root) {
        if (root == null) {
            return "#";
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        StringBuilder str = new StringBuilder();
        while (!queue.isEmpty()) {
            TreeNode poll = queue.poll();
            if (poll != null) {
                str.append("_").append(poll.val);
            } else {
                str.append("_#");
            }
            if (poll != null) {
                queue.add(poll.left);
                queue.add(poll.right);
            }
        }
        return str.toString().substring(1);
    }
    public static TreeNode Deserialize(String str) {
        if (str == null || str.isEmpty() || str.trim().isEmpty()) {
            return null;
        }
        if ("#".equals(str)) {
            return null;
        }
        String[] s = str.split("_");
        TreeNode tree  = new TreeNode(Integer.parseInt(s[0]));
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(tree);
        for(int i = 1 ; i < s.length - 1 ; i += 2) {
            TreeNode poll = queue.poll();
            if (poll != null) {
                if (!"#".equals(s[i])) {
                    poll.left = new TreeNode(Integer.parseInt(s[i]));
                    queue.add(poll.left);
                }

                if (!"#".equals(s[i + 1])) {
                    poll.right = new TreeNode(Integer.parseInt(s[i + 1]));
                    queue.add(poll.right);
                }
            }
        }
        return tree;
    }
}
