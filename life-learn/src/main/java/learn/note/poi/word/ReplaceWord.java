package learn.note.poi.word;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Wang WenLei
 * @Date 2022/11/17 22:32
 * @Version 1.0
 **/
public class ReplaceWord {
    public static void main(String[] args) {
        WordReporter wr = new WordReporter("fistWord.docx");
        WordReporter.ReplaceEntity data = new WordReporter.ReplaceEntity();
        Map<String, Object> textMap = new HashMap<>();
        textMap.put("firstParty","替换甲方");
        textMap.put("secondParty","替换乙方");
        data.setTextMap(textMap);
        try {
            wr.disposeWord(data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
