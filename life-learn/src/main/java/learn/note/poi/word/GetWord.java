package learn.note.poi.word;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @version 1.0
 * @author: Wang WenLei
 * @date: 2022/11/17 17:56
 */
public class GetWord {
	public static void main(String[] args) {
		String path = "D:\\life\\fistWord.docx";
		if (path.endsWith(".doc")) {
			try (InputStream is = new FileInputStream(path)) {

			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		} else if (path.endsWith(".docx")) {
			try (InputStream is = new FileInputStream(path)) {
				XWPFDocument docx = new XWPFDocument(is);


				System.out.println(docx.getBodyElements().size());

				XWPFParagraph first = docx.getParagraphs().get(1);
				XWPFRun header = first.insertNewRun(0);
				header.setText("甲方公司：");

				header.addBreak();

				XWPFParagraph two = docx.getParagraphs().get(1);
				System.out.println(two.getText());

				XWPFWordExtractor we = new XWPFWordExtractor(docx);
				String text = we.getText();
				System.out.println(text);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			System.out.println("此文件不是word文件");
		}
	}
}
