package learn.note.poi.word;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.*;

/**
 * @version 1.0
 * @author: Wang WenLei
 * @date: 2022/11/17 17:26
 */
public class NewWord {
	public static void main(String[] args) {
		File file = new File("fistWord.docx");
		try (XWPFDocument document = new XWPFDocument();
			 FileOutputStream out = new FileOutputStream(file)) {
			document.getParagraphPos(1);
			// 创建段落
			XWPFParagraph paragraph = document.createParagraph();
			// 使用运行输入文本或任何对象元素。
			XWPFRun run = paragraph.createRun();
			run.setText("输入一些文字");

			document.write(out);
		} catch (IOException e) {

		}
		System.out.println("createdocument.docx written successully");
	}
}
