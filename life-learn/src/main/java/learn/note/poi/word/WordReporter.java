package learn.note.poi.word;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;


import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author Wang WenLei
 * @Date 2022/11/17 21:49
 * @Version 1.0
 **/
public class WordReporter {
    private String tempLocalPath;
    private XWPFDocument xwpfDocument = null;
    private FileInputStream inputStream = null;
    private OutputStream outputStream = null;

    public WordReporter() {

    }

    public WordReporter(String tempLocalPath) {
        this.tempLocalPath = tempLocalPath;
    }

    /**
     * 设置模板路径
     *
     * @param tempLocalPath
     */
    public void setTempLocalPath(String tempLocalPath) {
        this.tempLocalPath = tempLocalPath;
    }

    /**
     * 初始化
     *
     * @throws IOException
     */
    private void init() throws IOException {
        inputStream = new FileInputStream(this.tempLocalPath);
        xwpfDocument = new XWPFDocument(inputStream);
    }

    /**
     * 处理方法方法
     *
     * @param data 需要替换的数据
     * @return
     * @throws Exception
     */
    public boolean disposeWord(ReplaceEntity data) throws Exception {
        init();
        if (data.getTextMap() != null && !data.getTextMap().isEmpty()) {
            replaceParams(xwpfDocument, data.getTextMap());
        }
        XWPFWordExtractor we = new XWPFWordExtractor(xwpfDocument);
        String text = we.getText();
        System.out.println(text);
        this.close(inputStream);
        return true;
    }



    /**
     * 收尾方法
     *
     * @param outDocPath
     * @return
     * @throws IOException
     */
    private boolean generate(String outDocPath) throws IOException {
        outputStream = Files.newOutputStream(Paths.get(outDocPath));
        xwpfDocument.write(outputStream);
        this.close(outputStream);
        this.close(inputStream);
        return true;
    }

    /**
     * 替换段落里面的变量
     *
     * @param doc    要替换的文档
     * @param params 参数
     */
    private static void replaceParams(XWPFDocument doc, Map<String, Object> params) {
        Iterator<XWPFParagraph> iterator = doc.getParagraphsIterator();
        XWPFParagraph paragraph;
        while (iterator.hasNext()) {
            paragraph = iterator.next();
            replaceParam(paragraph, params);
        }
    }

    /**
     * @param doc        docx解析对象
     * @param params     需要替换的信息集合
     * @param tableIndex 第几个表格
     * @param tableList  需要插入的表格信息集合
     */
    public static void changeTable(XWPFDocument doc, Map<String, Object> params, int tableIndex, List<String[]> tableList) {
        //获取表格对象集合
        List<XWPFTable> tables = doc.getTables();
        //获取第一个表格   根据实际模板情况 决定去第几个word中的表格
        XWPFTable table = tables.get(tableIndex);
        //替换表格中的参数
        replaceTableParams(doc, params);
        //在表格中插入数据
        insertTable(table, tableList);
    }

    /**
     * 替换表格里面的变量
     *
     * @param doc    要替换的文档
     * @param params 参数
     */
    private static void replaceTableParams(XWPFDocument doc, Map<String, Object> params) {
        Iterator<XWPFTable> iterator = doc.getTablesIterator();
        XWPFTable table;
        List<XWPFTableRow> rows;
        List<XWPFTableCell> cells;
        List<XWPFParagraph> paras;
        while (iterator.hasNext()) {
            table = iterator.next();
            //判断表格是需要替换还是需要插入，判断逻辑有$为替换，表格无$为插入
            if (matcher(table.getText()).find()) {
                rows = table.getRows();
                for (XWPFTableRow row : rows) {
                    cells = row.getTableCells();
                    for (XWPFTableCell cell : cells) {
                        paras = cell.getParagraphs();
                        for (XWPFParagraph para : paras) {
                            replaceParam(para, params);
                        }
                    }
                }
            }

        }
    }

    /**
     * 为表格插入行数，此处不处理表头，所以从第二行开始
     *
     * @param table     需要插入数据的表格
     * @param tableList 插入数据集合
     */
    private static void insertTable(XWPFTable table, List<String[]> tableList) {
        //创建与数据一致的行数
        for (int i = 0; i < tableList.size(); i++) {
            table.createRow();
        }
        int length = table.getRows().size();
        for (int i = 1; i < length; i++) {
            XWPFTableRow newRow = table.getRow(i);
            List<XWPFTableCell> cells = newRow.getTableCells();
            for (int j = 0; j < cells.size(); j++) {
                XWPFTableCell cell = cells.get(j);
                cell.setText(tableList.get(i - 1)[j]);
            }
        }
    }

    /**
     * 替换段落里面的变量
     *
     * @param paragraph 要替换的段落
     * @param params    参数
     */
    private static void replaceParam(XWPFParagraph paragraph, Map<String, Object> params) {
        List<XWPFRun> runs;
        Matcher matcher;
        String runText = "";

        if (matcher(paragraph.getParagraphText()).find()) {
            runs = paragraph.getRuns();
            int j = runs.size();
            for (int i = 0; i < j; i++) {
                runText += runs.get(0).toString();
                //保留最后一个段落，在这段落中替换值，保留段落样式
                if (!((j - 1) == i)) {
                    paragraph.removeRun(0);
                }
            }
            matcher = matcher(runText);
            if (matcher.find()) {
                while ((matcher = matcher(runText)).find()) {
                    runText = matcher.replaceFirst(String.valueOf(params.get(matcher.group(1))));
                }
                runs.get(0).setText(runText, 0);
            }
        }

    }

    /**
     * 正则匹配字符串
     *
     * @param str
     * @return
     */
    private static Matcher matcher(String str) {
        return Pattern.compile("\\$\\{(.+?)\\}", Pattern.CASE_INSENSITIVE).matcher(str);
    }


    /**
     * 关闭输入流
     *
     * @param is
     */
    private void close(InputStream is) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 关闭输出流
     *
     * @param os
     */
    private void close(OutputStream os) {
        if (os != null) {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static class ReplaceEntity {
        /**
         * 需替换的文本的数据
         */
        private Map<String, Object> textMap;
        /**
         * 表格里的数据列表
         */
        private List<Map<String, String>> tableList;
        /**
         * 需替换的第几个表格的下标
         */
        private int tableIndex;

        public Map<String, Object> getTextMap() {
            return textMap;
        }

        public void setTextMap(Map<String, Object> textMap) {
            this.textMap = textMap;
        }

        public List<Map<String, String>> getTableList() {
            return tableList;
        }

        public void setTableList(List<Map<String, String>> tableList) {
            this.tableList = tableList;
        }

        public int getTableIndex() {
            return tableIndex;
        }

        public void setTableIndex(int tableIndex) {
            this.tableIndex = tableIndex;
        }
    }
}
