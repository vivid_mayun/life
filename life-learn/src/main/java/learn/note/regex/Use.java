package learn.note.regex;

import java.util.regex.Pattern;

/**
 * @author WangWenLei
 * @DATE: 2022/2/8
 **/
public class Use {
    public static void main(String[] args) {
        String a = "中国-广东-abc-500";
        String b = "abc-500";
        String pattern = ".*" + b + ".*";
        boolean isMatches = Pattern.matches(b, a);
        System.out.println("a是否包含b?" + isMatches);
    }
}
