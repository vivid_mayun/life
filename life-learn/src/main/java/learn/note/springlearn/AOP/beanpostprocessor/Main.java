package learn.note.springlearn.AOP.beanpostprocessor;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author Wang WenLei
 * @Date 2022/7/16 15:06
 * @Version 1.0
 **/
public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("service-learn.xml");
        Test test = ac.getBean(Test.class);
        System.out.println(test.toString());
        // 关闭销毁
        ac.registerShutdownHook();
    }
}
