package learn.note.springlearn.AOP.beanpostprocessor;

/**
 * @Author Wang WenLei
 * @Date 2022/7/16 15:04
 * @Version 1.0
 **/
public class Test {
    private String name;

    public Test() {
        System.out.println("Test 调用了构造函数");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("调用了setName方法");
        this.name = name;
    }

    public void start() {
        System.out.println("自定义初始化的方法....");
    }

    @Override
    public String toString() {
        return "Test{" + "name='" + name + '\'' + '}';
    }
}
