package learn.note.springlearn.AOP;

/**
 * @author WangWenLei
 * @DATE: 2020/9/30
 **/
public interface Animal {
    void bark();
}
