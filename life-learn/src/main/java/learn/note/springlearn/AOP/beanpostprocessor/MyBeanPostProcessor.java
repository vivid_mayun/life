package learn.note.springlearn.AOP.beanpostprocessor;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;

import java.lang.reflect.Method;

/**
 * @Author Wang WenLei
 * @Date 2022/7/16 14:54
 * @Version 1.0
 **/
public class MyBeanPostProcessor implements InstantiationAwareBeanPostProcessor {
    /**
     * BeanPostProcessor接口中的方法
     * 在Bean的自定义初始化方法之前执行
     * Bean对象已经存在了
     */
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println(">>postProcessBeforeInitialization");
        return bean;
    }

    /**
     * BeanPostProcessor接口中的方法
     * 在Bean的自定义初始化方法执行完成之后执行
     * Bean对象已经存在了
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("<<postProcessAfterInitialization");
        return bean;
    }
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        System.out.println("执行了  postProcessBeforeInstantiation --->");
//        if (beanClass == Test.class) {
//            Enhancer e = new Enhancer();
//            e.setSuperclass(beanClass);
//            e.setCallback(new MethodInterceptor() {
//                @Override
//                public Object intercept(Object obj, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
//                    System.out.println("目标方法执行前:" + method);
//                    Object object = methodProxy.invokeSuper(obj, objects);
//                    System.out.println("目标方法执行后:" + method);
//                    return object;
//                }
//            });
//
//            Test test = (Test)e.create();
//            return test;
//        }
        return null;
    }

    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException {
        System.out.println("执行了  postProcessAfterInstantiation --->");
        return InstantiationAwareBeanPostProcessor.super.postProcessAfterInstantiation(bean, beanName);
    }

    @Override
    public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
        System.out.println("执行了  postProcessProperties --->");
        return InstantiationAwareBeanPostProcessor.super.postProcessProperties(pvs, bean, beanName);
    }
}
