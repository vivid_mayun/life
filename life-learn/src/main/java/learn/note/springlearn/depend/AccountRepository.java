package learn.note.springlearn.depend;

import org.springframework.stereotype.Component;

/**
 * @author WangWenLei
 * @DATE: 2020/11/4
 **/
@Component
public interface AccountRepository {
}
