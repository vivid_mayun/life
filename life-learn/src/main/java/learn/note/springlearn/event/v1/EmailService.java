package learn.note.springlearn.event.v1;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import java.util.List;

/**
 * 发布者
 * @author: Wang WenLei
 * @create: 2022-05-09 12:11
 **/
public class EmailService implements ApplicationEventPublisherAware {
    private List<String> blockedList;
    private ApplicationEventPublisher publisher;

    public void setBlockedList(List<String> blockedList) {
        this.blockedList = blockedList;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    public void sendEmail(String address,String content) {
        if (blockedList.contains(address)) {
            publisher.publishEvent(new BlockedListEvent(this,address,content));
            return;
        }
    }
}
