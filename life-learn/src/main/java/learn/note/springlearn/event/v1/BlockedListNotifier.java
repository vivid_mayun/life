package learn.note.springlearn.event.v1;

import org.springframework.context.ApplicationListener;

/**
 * 接收者
 * @author: Wang WenLei
 * @create: 2022-05-09 12:58
 **/
public class BlockedListNotifier implements ApplicationListener<BlockedListEvent> {
    private String notificationAddress;

    public void setNotificationAddress(String notificationAddress) {
        this.notificationAddress = notificationAddress;
    }

    @Override
    public void onApplicationEvent(BlockedListEvent event) {

    }
}
