package learn.note.springlearn.event.v1;

import org.springframework.context.ApplicationEvent;

/**
 * @author: Wang WenLei
 * @create: 2022-05-09 12:01
 **/
public class BlockedListEvent extends ApplicationEvent {
    private String address;
    private String content;

    /**
     * 创建一个 {@code BlockedListEvent}.
     *
     * @param source 事件最初发生的对象或与事件相关联的对象（从不为null ）
     * @param address 带一个参数，地址
     * @param content 带一个参数，内容
     */
    public BlockedListEvent(Object source, String address, String content) {
        super(source);
        this.address = address;
        this.content = content;
    }
}
