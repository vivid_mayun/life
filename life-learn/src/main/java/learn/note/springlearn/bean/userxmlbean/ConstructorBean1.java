package learn.note.springlearn.bean.userxmlbean;

public class ConstructorBean1 {
    private String name;
    private int age;

    private BelongToFirst sex;

    public ConstructorBean1(String name, int age,BelongToFirst sex) {
        System.out.println("ConstructorBean1 调用了构造方法");
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public BelongToFirst getSex() {
        return sex;
    }
}
