package learn.note.springlearn.bean.userxmlbean;

/**
 * 第一个XML的Bean对象的一个常量
 * @author WangWenLei
 * @DATE: 2021/4/9
 **/
public class BelongToFirst {
    private String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
