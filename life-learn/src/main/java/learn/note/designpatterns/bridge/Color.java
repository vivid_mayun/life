package learn.note.designpatterns.bridge;

/**
 * @ClassName Color
 * @Author wwl
 * @Date 2021/10/20 23:46
 * @Version 1.0
 **/
public interface Color {
    String name();
}
