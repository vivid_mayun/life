package learn.note.designpatterns.bridge;

public abstract class Traffic {
    private String producer = "wwl";
    public abstract String speed();
}
