package learn.note.designpatterns.bridge;

/**
 * @ClassName White
 * @Author wwl
 * @Date 2021/10/20 23:47
 * @Version 1.0
 **/
public class White implements Color {
    @Override
    public String name() {
        return "白色";
    }
}
