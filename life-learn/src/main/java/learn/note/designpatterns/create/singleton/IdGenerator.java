package learn.note.designpatterns.create.singleton;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 线程唯一单例
 * @author: Wang WenLei
 * @create: 2022-10-10 09:42
 **/
public class IdGenerator {
    private AtomicLong id = new AtomicLong(0);

    private static final ConcurrentHashMap<Long, IdGenerator> instance = new ConcurrentHashMap<>();

    private IdGenerator() {}

    private static IdGenerator getInstance() {
        Long currentThreadId = Thread.currentThread().getId();
        instance.putIfAbsent(currentThreadId,new IdGenerator());
        return instance.get(currentThreadId);
    }

    private long getId() {
        return id.incrementAndGet();
    }
}
