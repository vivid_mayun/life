package learn.note.designpatterns.observer;

/**
 * @Author Wang WenLei
 * @Date 2022/10/16 8:26
 * @Version 1.0
 **/
public class BinaryObserver extends Observer{

    public BinaryObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    protected void update() {
        System.out.println( "Binary String: " + Integer.toBinaryString( subject.getState() ) );
    }
}
