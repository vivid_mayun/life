package learn.note.designpatterns.observer;

/**
 * @Author Wang WenLei
 * @Date 2022/10/16 8:23
 * @Version 1.0
 **/
public abstract class Observer {
    protected Subject subject;
    protected abstract void update();
}
