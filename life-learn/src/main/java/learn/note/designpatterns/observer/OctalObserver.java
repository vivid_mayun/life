package learn.note.designpatterns.observer;

/**
 * @Author Wang WenLei
 * @Date 2022/10/16 8:28
 * @Version 1.0
 **/
public class OctalObserver extends Observer{
    public OctalObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Octal String: " + Integer.toOctalString( subject.getState() ) );
    }
}
