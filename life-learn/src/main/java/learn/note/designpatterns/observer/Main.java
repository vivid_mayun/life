package learn.note.designpatterns.observer;

/**
 * 观察者模式：定义一种一对多的依赖关系，当一个对象的状态发生变化时，所有依赖于它的对象都得到通知并被自动更新
 * @Author Wang WenLei
 * @Date 2022/10/16 8:32
 * @Version 1.0
 **/
public class Main {
    public static void main(String[] args) {
        Subject subject = new Subject();
        new BinaryObserver(subject);
        new OctalObserver(subject);
        System.out.println("First state change: 15");
        subject.setState(15);
        System.out.println("Second state change: 10");
        subject.setState(10);
    }
}
