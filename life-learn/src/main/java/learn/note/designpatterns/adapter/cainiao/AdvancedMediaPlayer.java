package learn.note.designpatterns.adapter.cainiao;

public interface AdvancedMediaPlayer {
    public void playVlc(String fileName);
    public void playMp4(String fileName);
}
