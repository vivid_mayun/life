package learn.note.designpatterns.adapter.cainiao;

/**
 * @author WangWenLei
 * @DATE: 2021/4/25
 **/
interface MediaPlayer {
    public void play(String audioType, String fileName);
}
