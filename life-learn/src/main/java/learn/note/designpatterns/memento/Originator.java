package learn.note.designpatterns.memento;

/**
 * @author: Wang WenLei
 * @create: 2022-10-17 16:25
 **/
public class Originator {
    private String state;
    public void setState(String state) {
        this.state = state;
    }
    public String getState() {
        return this.state;
    }

    public Memento saveStateToMemento() {
        return new Memento(this.state);
    }

    public void getStateFromMemento(Memento memento) {
        state = memento.getState();
    }
}
