package learn.note.designpatterns.memento;

/**
 * @author: Wang WenLei
 * @create: 2022-10-17 16:28
 **/
public class MementoPaneDems {
    public static void main(String[] args) {
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();
        originator.setState("state #1");
        originator.setState("state #2");
        careTaker.add(new Memento(originator.getState()));
        originator.setState("state #3");
        careTaker.add(new Memento(originator.getState()));
        originator.setState("state #4");
        System.out.println("CurrentState" + originator.getState());
        originator.getStateFromMemento(careTaker.get(0));
        System.out.println("Fist" + originator.getState());
    }
}
