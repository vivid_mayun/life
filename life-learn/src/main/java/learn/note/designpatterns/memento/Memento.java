package learn.note.designpatterns.memento;

/**
 * @author: Wang WenLei
 * @create: 2022-10-17 16:24
 **/
public class Memento {
    private String state;

    public Memento(String state) {
        this.state = state;
    }

    public void setState(String state) {
        this.state = state;
    }
    public String getState() {
        return this.state;
    }
}
