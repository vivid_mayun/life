package learn.note.designpatterns.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: Wang WenLei
 * @create: 2022-10-17 16:28
 **/
public class CareTaker {
    private List<Memento> mementoList = new ArrayList<>();
    public void add(Memento memento) {
        mementoList.add(memento);
    }
    public Memento get(int index) {
        return mementoList.get(index);
    }
}
