package learn.note.designpatterns.memento;

import com.google.gson.GsonBuilder;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author: Wang WenLei
 * @create: 2022-10-17 17:56
 **/
public class PathToMap {
    public static void main(String[] args) {
        List<String> pathList = Arrays.asList(
            "/etc/host",
            "/etc/kubernetes/ssl/certs",
            "/root"
        );
        Map<String, Map> mapTree = pathListToMap(pathList);
        System.out.println((new GsonBuilder().setPrettyPrinting().create()).toJson(mapTree));
    }

    /**
     * 路径列表转树形Map方法
     * @param pathList 需要处理的路径列表
     * @return 转换完成的树形对象
     */
    private static Map<String, Map> pathListToMap(List<String> pathList) {
        Map<String, Map> maps = new HashMap<>();
        if (pathList != null && !pathList.isEmpty()) {
            for (String path : pathList) {
                if (path != null && !path.isEmpty()) {
                    String[] pathItem = path.split("/");
                    // 去除空，防止出现 '//'的路径和排除开始截取出来的空字符串
                    List<String> pathItems = Arrays.stream(pathItem).filter(s -> !s.isEmpty()).collect(Collectors.toList());
                    disposePath(maps,pathItems,0);
                }
            }
        }
        return maps;
    }

    /**
     * 递归处理路径的方法，把目标路径逐层放入Map
     * @param maps 目标处理对象
     * @param path 每层的路径
     * @param index 需要处理的层次
     */
    private static void disposePath(Map<String, Map> maps,List<String> path ,int index) {
        if (path.size() <= index) {
            return;
        }

        Map pathMap = maps.get(path.get(index));
        if (pathMap == null) {
            maps.put(path.get(index),new HashMap<>());
        }
        disposePath(maps.get(path.get(index)),path,index+1);

    }
}
