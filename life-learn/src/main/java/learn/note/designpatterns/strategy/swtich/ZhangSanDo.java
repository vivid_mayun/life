package learn.note.designpatterns.strategy.swtich;

/**
 * @author WangWenLei
 * @DATE: 2021/4/14
 **/
public class ZhangSanDo implements Switch{
    @Override
    public void doSomething() {
        System.out.println("张三Do");
    }
}
