package learn.note.schedule;

import com.xxl.job.core.handler.IJobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @author: Wang WenLei
 * @date: 2022/11/17 15:10
 */
@Slf4j
@Component
public class XxlJobTestJobHandler extends IJobHandler {
	@Override
	public void execute() throws Exception {

	}
}
