package learn.note.schedule;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @version 1.0
 * @author: Wang WenLei
 * @date: 2022/11/17 15:44
 */
@Slf4j
@Component
public class XxlJobBean {
	/**
	 * 简单任务示例（Bean模式）
	 */
	@XxlJob("demoJobHandler")
	public ReturnT<String> demoJobHandler(String param) throws Exception {
		log.info("XXL-JOB {} " , param);
		return ReturnT.SUCCESS;
	}
}
