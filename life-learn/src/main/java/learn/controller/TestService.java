package learn.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shitao
 * @date 2022/2/11
 * 冻结余额service
 */
public class TestService {


    public static void main(String[] args) throws ParseException {
        class DTO {
            @JSONField(format = "yyyy-MM-dd")
            private Date createTime;
            private String orderId;
            private Integer id;


            public Date getCreateTime() {
                return createTime;
            }

            public void setCreateTime(Date createTime) {
                this.createTime = createTime;
            }

            public String getOrderId() {
                return orderId;
            }

            public void setOrderId(String orderId) {
                this.orderId = orderId;
            }


            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

        }
        String orderId = "dd-456";
        List<DTO> dtoList = new ArrayList<>();
        DTO dto0 = new DTO();
        dto0.setId(0);
        dto0.setOrderId("dd-789");
        dto0.setCreateTime(string2Date("20220223"));
        DTO dto1 = new DTO();
        dto1.setId(1);
        dto1.setOrderId("dd-456");
        dto1.setCreateTime(string2Date("20220223"));
        DTO dto2 = new DTO();
        dto2.setId(2);
        dto2.setOrderId("dd-456");
        dto2.setCreateTime(string2Date("20220201"));
        DTO dto3 = new DTO();
        dto3.setId(3);
        dto3.setOrderId("");
        dto3.setCreateTime(string2Date("20220102"));
        DTO dto4 = new DTO();
        dto4.setId(4);
        dto4.setOrderId("dd-789");
        dto4.setCreateTime(string2Date("20220101"));

        dtoList.add(dto0);
        dtoList.add(dto1);
        dtoList.add(dto2);
        dtoList.add(dto3);
        dtoList.add(dto4);
        //现在的排序是0 1 2 3 4
        //我希望 先对比73行的orderId 一致的放前面，一致的有多个则按时间正序，然后其他不一致的再按时间正序
        //排序完的顺序应该是 2 1 4 3 0
        System.out.println(JSON.toJSONString(dtoList));
//        List<DTO> collect = dtoList.stream()
//                .sorted(Comparator.comparing(DTO::getOrderId).thenComparing(DTO::getCreateTime))
//                .collect(Collectors.toList());

        List<DTO> collect = dtoList.stream()
                .sorted((v1,v2) -> {
                    if (v1.getOrderId().equals(orderId) && v2.getOrderId().equals(orderId)) {
                        return v1.getCreateTime().compareTo(v2.getCreateTime());
                    } else if(v1.getOrderId().equals(orderId)) {
                        return -1;
                    } else if(v2.getOrderId().equals(orderId)) {
                        return 1;
                    } else {
                        if (v1.getOrderId().compareTo(v2.getOrderId()) == 0) {
                            return v1.getCreateTime().compareTo(v2.getCreateTime());
                        } else {
                            return v1.getOrderId().compareTo(v2.getOrderId());
                        }
                    }
                })
                .collect(Collectors.toList());
        System.out.println(JSON.toJSONString(collect));

    }

    public static Date string2Date(String str) throws ParseException {
        return new SimpleDateFormat("yyyyMMdd").parse(str);
    }

}
