package wang.wenlei.lifekafkaproduce;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import wang.wenlei.lifekafkaproduce.config.KafkaSenderConfig;
import wang.wenlei.lifekafkaproduce.service.impl.TestServiceImpl;

@SpringBootApplication
public class KafkaProduceApplication {
	public static void main(String[] args) {
		SpringApplication.run(KafkaProduceApplication.class, args);
	}

	@Bean
	public ApplicationRunner runner (KafkaSenderConfig kafkaSenderConfig) {
		int i = 0;
		return args -> {
			kafkaSenderConfig.sendMessage("send massage number : " + i);
		};
	}
}
