package wang.wenlei.lifekafkaproduce.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import wang.wenlei.lifekafkaproduce.service.TestService;

import java.util.Optional;
import java.util.Properties;

/**
 * @author: Wang WenLei
 * @create: 2022-09-19 10:14
 **/
@Slf4j
@Service
public class TestServiceImpl implements TestService {

}
