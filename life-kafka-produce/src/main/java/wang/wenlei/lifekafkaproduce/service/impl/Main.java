package wang.wenlei.lifekafkaproduce.service.impl;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

/**
 * @author: Wang WenLei
 * @create: 2022-09-19 15:35
 **/
public class Main {
    public static void main(String[] args) {
        String bootstrapServers = "10.2.102.236:9092";
        Properties pro = new Properties();
        pro.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        pro.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        pro.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String,String> first_producer = new KafkaProducer<String, String>(pro);
        ProducerRecord<String,String> record = new ProducerRecord<>("my_first","Hello First visit kafuka");

        first_producer.send(record);
        first_producer.flush();
        first_producer.close();
    }
}
