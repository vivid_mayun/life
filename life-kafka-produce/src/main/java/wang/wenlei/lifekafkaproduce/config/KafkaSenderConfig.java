package wang.wenlei.lifekafkaproduce.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;

/**
 * @author: Wang WenLei
 * @create: 2022-09-19 10:31
 **/
@Configuration
public class KafkaSenderConfig {

    @Value("${kafka.topic.my-topic}")
    String myTopic;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    public void sendMessage(Object o) {
        kafkaTemplate.send(myTopic, o);
    }
}
