package wang.wenlei.lifespringcloudstock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LifeSpringCloudStockApplication {

    public static void main(String[] args) {
        SpringApplication.run(LifeSpringCloudStockApplication.class, args);
    }

}
