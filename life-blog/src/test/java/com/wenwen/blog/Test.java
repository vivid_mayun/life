package com.wenwen.blog;


import com.wenwen.blog.entity.User;
import com.wenwen.blog.entity.UserBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author WangWenLei
 * @DATE: 2021/2/2
 **/
public class Test {
    @org.junit.Test
    public void test(){
        String url = "![image.png](http://img.poesy.ink/FpzYIZniS7GQf-S6CHNGcQ2cEFGS)";
        String test = "# 0.前言\n" +
                "经过一段时间的探索，前端后端都有大致的样子了。下面就是部署到服务器，让我的博客项目公布在网上啦~~\n" +
                "![image.png](http://img.poesy.ink/FpzYIZniS7GQf-S6CHNGcQ2cEFGS)\n" +
                "# 1.前端部署\n" +
                "## 1.1服务器准备\n" +
                "Nginx环境\n" +
                "后端接口——想要完全的效果提前部署好\n" +
                "## 1.2前端打包准备\n" +
                "终端运行命令 npm run build\n" +
                "其实它就是个静态文件，在哪访问都一样\n" +
                "### 1.2.1 静态路径问题\n" +
                "点击index.html，通过浏览器运行，出现以下报错，如图\n"
                ;
        Pattern r = Pattern.compile("!\\[[a-z1-9A-Z.]+\\]\\([a-z1-9A-Z.:/\\-_]+\\)");
        /**
         * 七牛返回图片链接正则匹配![image.png](http://img.poesy.ink/FrSZWRY7lUNOBhiQEDt_JNelta3n)
         */
        Pattern r1 = Pattern.compile("\\([a-z1-9A-Z.:/\\-_]+\\)");
        // 现在创建 matcher 对象
        Matcher m = r.matcher(url);
        if (m.find()) {
            Matcher m1 = r1.matcher(m.group(0));
            if(m1.find()){
                String imgUrl = m1.group(0);
                System.out.println(imgUrl.substring(1, imgUrl.length() - 1));
            }
        }
    }
}
