package com.wenwen.blog.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @Author Wang WenLei
 * @Date 2022/5/19 23:40
 * @Version 1.0
 **/
@Service
public class RedisBloomFilter {
    @Autowired
    private RedisTemplate redisTemplate;

    public static final String BLOOMFILTER_NAME = "bloom-filter-test";

    /**
     * 添加元素
     * @param id
     * @return
     */
    public Boolean bloomFilterAdd(Integer id){
        DefaultRedisScript<Boolean> LuaScript = new DefaultRedisScript<Boolean>();
        LuaScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("bf_add.lua")));
        LuaScript.setResultType(Boolean.class);
        //封裝傳遞腳本參數
        ArrayList<Object> params = new ArrayList<>();
        params.add(BLOOMFILTER_NAME);
        params.add(String.valueOf(id));
        return (Boolean) redisTemplate.execute(LuaScript, params);
    }

    /**
     * 是否存在
     * @param id
     * @return
     */
    public Boolean bloomFilterExist(Integer id){
        DefaultRedisScript<Boolean> LuaScript = new DefaultRedisScript<Boolean>();
        LuaScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("bf_exist.lua")));
        LuaScript.setResultType(Boolean.class);
        //封裝傳遞腳本參數
        ArrayList<Object> params = new ArrayList<>();
        params.add(BLOOMFILTER_NAME);
        params.add(String.valueOf(id));
        return (Boolean) redisTemplate.execute(LuaScript, params);
    }

}
