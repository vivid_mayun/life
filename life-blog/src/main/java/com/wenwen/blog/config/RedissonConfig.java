package com.wenwen.blog.config;

import org.redisson.Redisson;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Wang WenLei
 * @Date 2022/5/18 15:15
 * @Version 1.0
 **/
@Configuration
public class RedissonConfig {

    @Bean
    public Redisson redisson () {
        Config config = new Config();

//        //单机模式
//        config.useSingleServer()
//            //配置地址
//            .setAddress("redis://150.158.58.15:6381")
//            // 配置密码
//            .setPassword("redis2716")
//            // 配置数据库
//            .setDatabase(0);
        config.useSentinelServers()
                .addSentinelAddress("redis://150.158.58.15:26379","redis://150.158.58.15:26380","redis://150.158.58.15:26381")
                .setMasterName("mymaster")
                .setPassword("redis2716");
        return (Redisson) Redisson.create(config);
    }
}
