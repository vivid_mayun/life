package com.wenwen.blog.controller.admin;

import com.wenwen.blog.entity.User;
import com.wenwen.blog.entity.response.UserResponse;
import com.wenwen.blog.mapper.UserMapper;
import com.wenwen.blog.service.IUserService;
import com.wenwen.blog.util.EnDecoderUtil;
import com.wenwen.blog.util.JwtUtil;
import com.wenwen.blog.util.response.ResponseDataBase;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author WangWenLei
 * @DATE: 2020/11/5
 **/

@RestController
public class Login {

    @Autowired
    UserMapper userMapper;

    @Autowired
    IUserService iUserService;

    @Autowired
    private JwtUtil jwtUtil;

//    @GetMapping("/listUser")
    public String listUser(){
        final List<Map<String, String>> maps = userMapper.listUser();
        final Map<String, String> stringStringMap = userMapper.listUserOne();
        System.out.println(maps);
        System.out.println(stringStringMap);
        return "";
    }

    @ApiOperation("用户登录接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName",value = "loginName"),
            @ApiImplicitParam(name = "password" ,value = "password")
    })
    @PostMapping("/login")
    public ResponseDataBase<UserResponse> login(@RequestBody User user){
        ResponseDataBase<UserResponse> response = new ResponseDataBase<>();
        if(StringUtils.isBlank(user.getLoginName())){
            response.fail("请输入用户名！");
            return response;
        }
        if(StringUtils.isBlank(user.getUserPassword())){
            response.fail("请输入密码！");
            return response;
        }
        return iUserService.login(user.getLoginName(), user.getUserPassword());
    }


    @RequestMapping(value = "/login2", method = RequestMethod.POST)
    public ResponseDataBase<String> login2(@RequestParam(name = "userName") String userName,
                             @RequestParam(name = "passWord") String passWord){
        String jwt = "";
        ResponseDataBase<String> response = new ResponseDataBase<>();

        try {
            jwt = iUserService.login2(userName, passWord);
            response.setData(jwt);
            response.successful();
            return response;
        } catch (Exception e) {
            response.fail();
            return response;
        }
    }

    @GetMapping("/test2")
    public Map<String, Object> test2(@RequestParam("jwt") String jwt) {
        //这个步骤可以使用自定义注解+AOP编程做解析jwt的逻辑，这里为了简便就直接写在controller里
        Claims claims = jwtUtil.parseJWT(jwt);
        String name = claims.get("name", String.class);
        String age = claims.get("age", String.class);
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("age", age);
        map.put("code", "0");
        map.put("msg", "请求成功");
        return map;
    }
}
