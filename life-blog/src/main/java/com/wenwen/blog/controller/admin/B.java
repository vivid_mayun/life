package com.wenwen.blog.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author Wang WenLei
 * @Date 2022/7/2 10:48
 * @Version 1.0
 **/
@Component
public class B {
    @Autowired
    private A a;
}
