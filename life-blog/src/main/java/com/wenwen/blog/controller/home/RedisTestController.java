package com.wenwen.blog.controller.home;

import org.redisson.Redisson;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RLock;
import com.wenwen.blog.util.RedisBloomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Author Wang WenLei
 * @Date 2022/5/17 14:54
 * @Version 1.0
 **/
@RestController
public class RedisTestController {

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    Redisson redisson;




    @GetMapping("/de")
    public String test () {
        UUID uuid = UUID.randomUUID();
        // 加一个10秒钟自动删除键的设置，来防止宕机后锁无法删除问题
        Boolean lock_name = redisTemplate.opsForValue().setIfAbsent("lock_name", uuid,10L, TimeUnit.SECONDS);
        if (lock_name) {
            try {
                Object name = redisTemplate.opsForValue().get("name");
                if (name != null) {
                    redisTemplate.opsForValue().set("name",(int)name - 1);
                } else {
                    redisTemplate.opsForValue().set("name",10);
                }

            } finally {
                // 删除之前先获取，如果uuid同，表示为同一个客户端或线程的处理。
                // 需要注意一点：比较和删除他们不是一个原子操作，这做降低一些概率
                if (uuid.equals(redisTemplate.opsForValue().get("lock_name"))) {
                    // 完成操作删除锁，为了防止执行业务中间有异常抛出执行不了放在finally里
                    redisTemplate.delete("lock_name");
                }
            }
            return "去库存成功！" ;

        } else {
            return "繁忙，请稍后重试！" ;
        }
    }

    @GetMapping("/de2")
    public String test2 () {
        RLock lock_name = redisson.getLock("lock_name");
        try {
            lock_name.lock(10L,TimeUnit.SECONDS);
            int name = (int)redisTemplate.opsForValue().get("name");
            redisTemplate.opsForValue().set("name",name - 1);
        } finally {
            lock_name.unlock();
        }
        return "去库存成功！" ;
    }

    @GetMapping("/bloomFilterCreate")
    public String bloomFilterCreate () {
        System.out.println("开始创建");
        final RBloomFilter<Object> bloomFilter = redisson.getBloomFilter("bloom-filter-test");
        bloomFilter.tryInit(10000, 0.0001);
        for (int i = 0 ; i < 1000 ; i++) {
            bloomFilter.add(i);
        }
        return "bloomFilterCreate Over！" ;
    }

    @GetMapping("/bloomFilterTest")
    public String bloomFilterTest (@RequestParam Integer id) {
        final RBloomFilter<Object> bloomFilter = redisson.getBloomFilter("bloom-filter-test");
        if (bloomFilter.contains(id)) {
            return ("bloomFilterTest Over！id存在:" + id);
        } else {
            return ("bloomFilterTest Over！id不存在:" + id);
        }
    }

    @GetMapping("/luaTest")
    public Boolean luaTest () {
        DefaultRedisScript<Boolean> luaScript = new DefaultRedisScript<>();
        luaScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("key_add.lua")));
        luaScript.setResultType(Boolean.class);
        //封装传递脚本参数
        List<String> params = new ArrayList<>();
        params.add("lua-test");
        params.add("1001");
        return (Boolean)redisTemplate.execute(luaScript,params);
    }

    @GetMapping("/addBloomFilterLua")
    public Boolean bloomFilterLua (@RequestParam Integer id) {
        DefaultRedisScript<Boolean> luaScript = new DefaultRedisScript<>();
        luaScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("bf_add.lua")));
        luaScript.setResultType(Boolean.class);
        //封装传递脚本参数
        List<String> params = new ArrayList<>();
        params.add("bloom-filter-test");
        params.add("1001");
        return (Boolean)redisTemplate.execute(luaScript,params);
    }

    @GetMapping("/exitBloomFilterLua")
    public Boolean exitBloomFilterLua (@RequestParam Integer id) {
        DefaultRedisScript<Boolean> LuaScript = new DefaultRedisScript<>();
        LuaScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("bf_exist.lua")));
        LuaScript.setResultType(Boolean.class);
        //封装传递脚本参数
        ArrayList<String> params = new ArrayList<>();
        params.add("bloom-filter-test");
        params.add(String.valueOf(id));
        return (Boolean) redisTemplate.execute(LuaScript, params);
    }


    @Autowired
    private RedisBloomFilter redisBloomFilterService;

    @GetMapping("/addUser/{id}")
    public boolean addUser(@PathVariable("id") Integer id){
        return redisBloomFilterService.bloomFilterAdd(id);
    }

    @GetMapping("/existUser/{id}")
    public boolean existUser(@PathVariable("id") Integer id){
        return redisBloomFilterService.bloomFilterExist(id);
    }

    @GetMapping("/pfadd")
    public boolean pfadd(){
        Long add = redisTemplate.opsForHyperLogLog().add("20220525", "user5", "user6");
        System.out.println("add:" + add);
        Long size = redisTemplate.opsForHyperLogLog().size("20220525");
        System.out.println("size" + size);
        redisTemplate.opsForHyperLogLog().delete("20220525");
        return true;
    }

    @GetMapping("/limit")
    public String limit(){
        if (isActionAllowed("test",15,30,60,1)) {
            return "通行~";
        } else {
            return "繁忙请稍后";
        }
    }

    private boolean isActionAllowed(String userId,String actionKey,int perid,int maxCount) {
        String key = String.format("hist:%s:%s",userId,actionKey);
        long nowTs = System.currentTimeMillis();
        redisTemplate.multi();
        try {
//            List list = redisTemplate.executePipelined(new SessionCallback<Long>() {
//                @Override
//                public Long execute(RedisOperations operations) throws DataAccessException {
//                    operations.opsForZSet().add(key, nowTs, nowTs);
//                    redisTemplate.opsForZSet().removeRangeByScore(key, 0, nowTs - perid * 1000);
//                    return redisTemplate.opsForZSet().zCard(key);
//                }
//            });
            redisTemplate.opsForZSet().add(key, nowTs, nowTs);
            redisTemplate.opsForZSet().removeRangeByScore(key, 0, nowTs - perid * 1000);
            Long list = redisTemplate.opsForZSet().zCard(key);
            redisTemplate.exec();
            return list <= maxCount;
        } catch (Exception e) {
            redisTemplate.discard();
            return false;
        }
    }

    /**
     * lua 脚本
     */
    private static final String LUA_SCRIPT = "return redis.call('cl.throttle',KEYS[1], ARGV[1], ARGV[2], ARGV[3], ARGV[4])";

    public boolean isActionAllowed(String key, int maxBurst, int countPerPeriod, int period ,int quantity) {
        DefaultRedisScript<List> script = new DefaultRedisScript<>(LUA_SCRIPT, List.class);
        List<Long> rst = (List<Long>) redisTemplate.execute(script, Arrays.asList(key), maxBurst, countPerPeriod, period, quantity);
        //这里只关注第一个元素0表示正常，1表示过载
        return rst.get(0) == 0;
    }

}
